import requests
from bs4 import BeautifulSoup, Tag, NavigableString
import re
import json
import time, datetime
import pymongo
import array
from pymongo import MongoClient
import random

# set mongo vars
client = MongoClient('localhost', 27017)
db = client.indoEuropean
collection = db["indoEuropeanCognates2"]
outfile=open("output.txt","a+", encoding="utf-8")
outfile.write("Test")
#set urls
rootURL = 'https://en.wiktionary.org'
pieNounsURL = 'https://en.m.wiktionary.org/wiki/Appendix:List_of_Proto-Indo-European_nouns'

def parseWiktionaryPage(wiktionaryURL, inputLanguageCode, inputWord, pieWord, inputMeaning):
    print("Input word:")
    print(inputWord)
    outputMeaning = inputMeaning
    request_object = requests.get(rootURL + wiktionaryURL)
    searchResponse = BeautifulSoup(request_object.content, 'html.parser')
    
    def parseDescendantList(descendants):
        for descendant in descendants:
            # if the word isn't borrowed, process
            if descendant.find("span", attrs={"title": ["borrowed", "derived by addition of morphemes"]}, recursive=False) == None:
                descendant_a = descendant.find("a")
                if descendant_a:
                    descendantLink = descendant_a.get("href")

                    try:
                        print(descendant_a.parent.get("class"))
                        print(descendant_a.parent.get("class")==["Latn"])
                        if descendant_a.parent.get("class")==["Latn"] or descendant_a.parent.get("class")==["Latinx"]:
                            descendantWord = descendant_a.contents[0]
                        else:
                            descendantWord = descendant.find(attrs={"class": ["Latinx", "Latn"]}).contents[0]
                        if descendant.find(attrs={"class": ["Latn", "Latinx"]}):
                            descendantLang = descendant.find(attrs={"class": ["Latn", "Latinx"]}).get("lang")
                            print(descendantLink)
                            print(descendantLang)
                            print(descendantWord)
                            print(pieWord)
                            print(outputMeaning)
                            try:
                                parseWiktionaryPage(descendantLink, descendantLang, descendantWord, pieWord, outputMeaning)
                            except: 
                                pass
                    except:
                        pass

    # first find transliterated headword
    headword = searchResponse.find(attrs={"class": "headword-tr"})
    if headword is None:
        print("no transliteration found")
        # find headword in latin script
        headword = searchResponse.find(attrs={"class": ["headword"], "lang": inputLanguageCode})

    if headword:       
        outputWord = headword.contents[0]

        # let's try and find the meaning
        if headword.find_next("li").find("a"):
            outputMeaning = headword.find_next("li").find("a").contents[0]
        else: # if we can't find a new meaning, assume the meaning hasn't changed
            outputMeaning = inputMeaning

        # if we're on PIE, pieWord is the headword
        if inputLanguageCode == "ine-pro":
            pieWord = outputWord

        # find list of descendants 
        # first if there's a next headword (i.e. other languages on that page)
        if headword.find_next(attrs={"class": ["headword", "headword-tr"]}):
            # only process descendants if they belong to the right language
            if headword.find_next(attrs={"id":"Descendants"}) != headword.find_next(attrs={"class": ["headword", "headword-tr"]}).find_next(attrs={"id":"Descendants"}):
                descendantHeader = headword.find_next(attrs={"id":"Descendants"})
                descendantList = descendantHeader.find_next("ul").find_all("li")
                try:
                    parseDescendantList(descendantList)
                except:
                    pass

       # if there are no more languages on that page, things are easier
        elif headword.find_next(attrs={"class": ["headword", "headword-tr"]}) is None:
            descendantHeader = headword.find_next(attrs={"id":"Descendants"})
            if descendantHeader:
                descendantList = descendantHeader.find_next("ul").find_all("li")
                parseDescendantList(descendantList)
    
    else:
        outputWord = inputWord
    # find IPA Transcription of headword
    try:
        if headword.find_previous(attrs={"class": "IPA"}) != headword.find_previous(attrs={"class": ["headword", "headword-tr"]}).find_previous(attrs={"class": "IPA"}):
            ipaTranscription = headword.find_previous(attrs={"class": "IPA"}).find_parent("ul").find(attrs={"class": "IPA"}).contents[0]
            #print(ipaTranscription)
        else:
            ipaTranscription = None
    except:
        ipaTranscription = None
        pass

    try:
        collection.insert({"lang": inputLanguageCode, "latinTranscription": outputWord, "ipaTranscription": ipaTranscription, "pieWord": pieWord, "meaning": outputMeaning})
        print("inserted " + outputWord + " for " + inputLanguageCode)
    except:
        pass

    searchResponse.decompose()
    return

lemmaURL = "https://en.wiktionary.org/w/index.php?title=Category:Proto-Indo-European_lemmas" 
#lemmaURL = "https://en.wiktionary.org/wiki/Reconstruction:Proto-Indo-Iranian/d%CA%B0%C3%A1%C7%B0%CA%B0ati"
lemma_request_object = requests.get(lemmaURL)
lemma_searchResponse = BeautifulSoup(lemma_request_object.content, 'html.parser')

parseWiktionaryPage("/wiki/Reconstruction:Proto-Indo-European/bʰardʰeh₂", "ine-pro", None, None, None)
print("inserted test word")
time.sleep(20)
# print(lemma_searchResponse)
#print(lemma_searchResponse.find("div", attrs={"class": "mw-content-ltr"}))
lemmas = lemma_searchResponse.find(attrs={"class": "mw-content-ltr"}).find_all("li")
for lemma in lemmas:
    print(lemma.find("a").contents[0])
    lemmaHref = lemma.find("a").get("href")
    lemmaURL = lemma.find("a").contents[0]
    parseWiktionaryPage(lemmaHref, "ine-pro", None, None, None)

print("DONE WITH INSERT, CHECK IT")



