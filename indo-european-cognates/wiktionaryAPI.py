import requests
from bs4 import BeautifulSoup, Tag, NavigableString
import re
import json
import time, datetime
import pymongo
import array
from pymongo import MongoClient
import random

# set mongo vars
client = MongoClient('localhost', 27017)
db = client.indoEuropean
collection = db["indoEuropeanCognates2"]
outfile=open("output.txt","a+", encoding="utf-8")
outfile.write("Test")
#set urls
rootURL = 'https://en.wiktionary.org'
pieNounsURL = 'https://en.m.wiktionary.org/wiki/Appendix:List_of_Proto-Indo-European_nouns'

# define function to parse a list of elements
def parseDescendantList(descendantList, parentLanguage, level, inputVar):
    # we're appending to the input
    returnVar = inputVar

    for child in descendantList:
        # print("-------------------------------------------------")
        # print("Child:")
        # print("-------------------------------------------------")
        # print(child)
        outfile.write(str(child))
        # print("-------------------------------------------------")
        # print("End of child:")
        # print("-------------------------------------------------")
        transcriptionLanguage = ""
        # if child has its own word, add that word and move onto descendants
        try:
            # transcription is always in a span with either Latn or Latinx as its class
            latinTranscriptions = child.find_all(attrs={"class": ["Latn", "Latinx"]}, recursive=False)
            for option in latinTranscriptions:
                # sometimes the transcription is inside an <a> element
                if option.find("a"):
                    transcriptionWord = option.contents[0].contents[0]
                else: # otherwise the transcription will be the contents of the span
                    transcriptionWord = option.contents[0]

                # get the language of this word
                transcriptionLanguage = option.get("lang")
                
                # if we can find a language, and the word isn't borrowed, append to return variable
                if transcriptionLanguage and child.find("span", attrs={"title": "borrowed"}, recursive=False) == None and child.contents[0][0] != "→":
                    
                    # only add word if there isn't already one for this language 
                    # (as the first word in a list is usually the most relevant)
                    if len(list(filter(lambda d: d['lang'] == transcriptionLanguage, returnVar))) == 0:
                        returnVar.append({"lang": transcriptionLanguage, "parentLang": parentLanguage, "latinTranscription": transcriptionWord})
            
                # if the transcription is a link, follow it and parse
                # print("pre option.find(a)")
                # print(option)
                if option.find("a") and level < 20:
                    # get the link URL
                    childURL = rootURL + option.find("a").get("href")
                    # reset hasDescendants, and only follow link if it has
                    # <i>see there for further descendants</i> as a sibling
                    hasDescendants = False
                    if option.parent.find_all("i"):
                        for i in option.parent.find_all("i"):
                            if i.contents[0] == "see there for further descendants":
                                hasDescendants = True

                    # print("PREDESCENDANTS")
                    # print(parentLanguage)
                    if hasDescendants:
                        # beautifulsoup stuff
                        childRequestObject = requests.get(childURL)
                        childSearchResponse = BeautifulSoup(childRequestObject.content, 'html.parser')
                        # find the first instance of descendants beneath this language's heading
                        childDescendants = childSearchResponse.find(attrs={"class": "headword", "lang": transcriptionLanguage}).parent.find_next("span", attrs={"id": ["Descendants", "Descendants_2"]}).parent.find_next("ul")
                        # recursively call this function to append this list of descendants to the return variable
                        returnVar = parseDescendantList(childDescendants, transcriptionLanguage, level + 1, returnVar)
                        outfile.write("SIBLINGS:")
                        outfile.write(childDescendants.next_sibling)
                        outfile.write(parentLanguage)
                        # print("HERE")
                        # print("childDescendants:")
                        # print(childDescendants)
                        # print("NEXT SIBLING:")
                        # # print(childDescendants.next_sibling.next_sibling)
                        # # #print(childDescendants.previous_sibling)
                        # print(childDescendants.find_next_sibling("ul"))
                        # # print("PARENT:")
                        # #print(childDescendants.parent)
                        # if childDescendants.find_next_sibling("ul"):
                        #     print("ACTUALLY GOT HERE")
                        #     childDescendants = childDescendants.find_next_sibling("ul")
                        #     returnVar = parseDescendantList(childDescendants, transcriptionLanguage, level, returnVar)
                        # else:
                        #     print("NO NEXT SIBLING, PRINTING PARENT:")
                        #     print(childDescendants.parent)

        except:
            # print("-------------------------------------------------")
            # print("Error:")
            # print("-------------------------------------------------")
            # print(e)
            # outfile.write(str(e))
            # print("-------------------------------------------------")
            # print("End of error:")
            # print("-------------------------------------------------")
            pass # bad practice, I know

        # work out if this descendant has a further list of descendants
        sublist = child.find("ul")
        # if it does have its own descendants, call this function recursively to append to return var
        if sublist and type(sublist) != int:
            if transcriptionLanguage == "":
                transcriptionLanguage = "placeholder"

            returnVar = parseDescendantList(sublist, transcriptionLanguage, level + 1, returnVar)
    # print("--------------------------------------------------")
    # print("Checking for siblings of:")
    # print("--------------------------------------------------")
    # print(descendantList)
    # print("--------------------------------------------------")
    # print("Next sibling type is: ")
    # print("--------------------------------------------------")
    # print(type(descendantList.next_sibling))
    # print(isinstance(descendantList.next_sibling, NavigableString))
    if isinstance(descendantList.next_sibling, NavigableString):
        # print("--------------------------------------------------")
        # print("Next NEXT sibling is: ")
        # print("--------------------------------------------------")
        if descendantList.next_sibling.next_sibling and descendantList.next_sibling.next_sibling.name == "ul":
            print("Next sibling weirdness")
            descendants = descendantList.next_sibling.next_sibling
            returnVar = parseDescendantList(descendants, parentLanguage, 0, returnVar)
    if descendantList.next_sibling and descendantList.next_sibling.name == "ul":
        print("Next sibling weirdness (2)")
        descendants = descendantList.next_sibling
        returnVar = parseDescendantList(descendants, parentLanguage, 0, returnVar)

    # find derived terms list (if it exists)
    print("-----------------------------------------")
    print("descendantList:")
    print("-----------------------------------------")
    print(descendantList)
    print("-----------------------------------------")
    print("Previous Sibling:")
    print("-----------------------------------------")
    print(descendantList.previous_sibling.previous_sibling.previous_sibling)
    try:
        print("Derived attempt")
        if descendantList and descendantList.previous_sibling and descendantList.previous_sibling.previous_sibling.previous_sibling and  descendantList.previous_sibling.previous_sibling.previous_sibling.name == "ul":
            print("Working on derived terms")
            derivedTerms = descendantList.previous_sibling.previous_sibling.previous_sibling
            returnVar = parseDescendantList(derivedTerms, parentLanguage, 0, returnVar)
    except: 
        pass

    return returnVar


def parseWiktionaryPage(wiktionaryURL, inputLanguageCode, inputWord, pieWord, inputMeaning):
    print(inputWord)
    outputMeaning = inputMeaning
    request_object = requests.get(rootURL + wiktionaryURL)
    searchResponse = BeautifulSoup(request_object.content, 'html.parser')
    
    # find headword in latin script
    headword = searchResponse.find(attrs={"class": ["headword", "headword-tr"], "lang": inputLanguageCode})
    if headword:       
        outputWord = headword.contents[0]

        # let's try and find the meaning
        if headword.find_next("li").find("a"):
            outputMeaning = headword.find_next("li").find("a").contents[0]
        else:
            outputMeaning = inputMeaning

        if inputLanguageCode == "ine-pro":
            pieWord = outputWord


        # find list of descendants
        if headword.find_next(attrs={"class": ["headword", "headword-tr"]}):
            if headword.find_next(attrs={"id":"Descendants"}) != headword.find_next(attrs={"class": ["headword", "headword-tr"]}).find_next(attrs={"id":"Descendants"}):
                descendantHeader = headword.find_next(attrs={"id":"Descendants"})
                descendantList = descendantHeader.find_next("ul").find_all("li")
                for descendant in descendantList:
                    #print(descendant)
                    descendant_a = descendant.find("a")
                    descendantLink = descendant_a.get("href")
                    descendantWord = descendant_a.contents[0]
                    try:
                        descendantLang = descendant.find(attrs={"class":["Latn", "Latinx"]}).get("lang")
                    except:
                        return
                    parseWiktionaryPage(descendantLink, descendantLang, descendantWord, pieWord, outputMeaning)
        elif headword.find_next(attrs={"class": ["headword", "headword-tr"]}) is None:
            descendantHeader = headword.find_next(attrs={"id":"Descendants"})
            if descendantHeader:
                descendantList = descendantHeader.find_next("ul").find_all("li")
                for descendant in descendantList:
                    #print(descendant)
                    descendant_a = descendant.find("a")
                    if descendant_a:
                        descendantLink = descendant_a.get("href")
                        descendantWord = descendant_a.contents[0]
                        if descendant.find(attrs={"class": ["Latn", "Latinx"]}):
                            descendantLang = descendant.find(attrs={"class": ["Latn", "Latinx"]}).get("lang")
                            parseWiktionaryPage(descendantLink, descendantLang, descendantWord, pieWord, outputMeaning)
    else:
        outputWord = inputWord
    # find IPA Transcription of headword
    try:
        if headword.find_previous(attrs={"class": "IPA"}) != headword.find_previous(attrs={"class": ["headword", "headword-tr"]}).find_previous(attrs={"class": "IPA"}):
            ipaTranscription = headword.find_previous(attrs={"class": "IPA"}).find_parent("ul").find(attrs={"class": "IPA"}).contents[0]
            print(ipaTranscription)
        else:
            ipaTranscription = None
    except:
        ipaTranscription = None
        pass

    try:
        collection.insert({"lang": inputLanguageCode, "latinTranscription": outputWord, "ipaTranscription": ipaTranscription, "pieWord": pieWord, "meaning": outputMeaning})
        print("inserted " + outputWord + " for " + inputLanguageCode)
    except:
        pass

    searchResponse.decompose()
    return

lemmaURL = "https://en.wiktionary.org/wiki/Category:Proto-Indo-European_lemmas" 
lemma_request_object = requests.get(lemmaURL)
lemma_searchResponse = BeautifulSoup(lemma_request_object.content, 'html.parser')

# print(lemma_searchResponse)
print(lemma_searchResponse.find("div", attrs={"class": "mw-content-ltr"}))
lemmas = lemma_searchResponse.find(attrs={"class": "mw-content-ltr"}).find_all("li")
for lemma in lemmas:
    print(lemma.find("a").contents[0])
    lemmaHref = lemma.find("a").get("href")
    lemmaURL = lemma.find("a").contents[0]
    parseWiktionaryPage(lemmaHref, "ine-pro", None, None, None)
parseWiktionaryPage("/wiki/Reconstruction:Proto-Indo-European/alb%CA%B0%C3%B3s#Proto-Indo-European", "ine-pro", "*albʰós", "*albʰós", "white")
#testVar = parseWiktionaryPage("/wiki/Reconstruction:Proto-Indo-European/*albʰós", "ine-pro", "h₁nómn̥", [])

print("DONE WITH INSERT, CHECK IT")



time.sleep(50)
################ For troubleshooting a particular word, uncomment the block below and replace URL ################
# testURL = 'https://en.m.wiktionary.org/wiki/Reconstruction:Proto-Slavic/j%D1%8Cm%C4%99'
# requestObject = requests.get(testURL)
# searchResponse = BeautifulSoup(requestObject.content, 'html.parser')
# descendants = searchResponse.find("span", attrs={"id": "Descendants"}).parent.find_next("ul")
# print("-------------------------------------------------")
# print("descendant sibling:")
# print("-------------------------------------------------")
# print(descendants.next_sibling.name)
# print("-------------------------------------------------")
# print("End of descendant sibling:")
# print("-------------------------------------------------")
# print(parseDescendantList(descendants, "ine-pro", 0, []))
# while descendants.next_sibling and descendants.next_sibling.name == "ul":
#     descendants = descendants.next_sibling
#     print(parseDescendantList(descendants, "ine-pro", 1, []))
    
# time.sleep(5)

# go to PIE nouns URL and fetch all root/meaning pairs
# pieRequestObject = requests.get(pieNounsURL)
# pieSearchResponse = BeautifulSoup(pieRequestObject.content, 'html.parser')
# nounTables = pieSearchResponse.find_all("table")
# for nounTable in nounTables: # for each table on that page
#     nounRows = nounTable.find_all("tr") # create list of rows
#     for row in nounRows: # for each row
#         if row.find("td"): # if this row has a cell
#             cells = row.find_all("td") # create list of cells
#             pieWord = cells[0].find("a").contents[0] # word is in the contents of first cell
#             cognates = [{"lang": "ine-pro", "latinTranscription": pieWord}] # initiate cognates
#             pieWordURL = rootURL + cells[0].find("a").get("href") # get url of PIE word
#             meaningCell = cells[1] # meaning is the second cell
#             if meaningCell.find("a"): 
#                 meaning = meaningCell.find("a").contents[0]
#             else:
#                 meaning = cells[1].contents[0]

#             # request the URL of this PIE word
#             #pieWordURL = "https://en.m.wiktionary.org/wiki/Reconstruction:Proto-Indo-European/n%C3%A9h%E2%82%82s"
#             request_object = requests.get(pieWordURL)
#             searchResponse = BeautifulSoup(request_object.content, 'html.parser')
#             print(pieWord)
#             outfile.write(pieWord)
#             if searchResponse.find("span", attrs={"id": "Descendants"}) and pieWord=="*néh₂s-": # find descendants list
#                 descendants = searchResponse.find("span", attrs={"id": "Descendants"}).parent.find_next("ul")
#                 # if link has "new" class then page does not exist
#                 if cells[0].find("a").get("class") != "new":
#                     outfile.write("Working on " + pieWord)
#                     print("Working on " + pieWord)
#                     cognates = parseDescendantList(descendants, "ine-pro", 0, cognates) # parse list of descendants
#                     while descendants.next_sibling and descendants.next_sibling.name == "ul":
#                         descendants = descendants.next_sibling
#                         cognates = parseDescendantList(descendants, "ine-pro", 0, cognates)
                    
#                     for word in cognates:
#                         try:
#                             collection.update({"pieWord": pieWord, "languageCode": word.lang}, {"$set": {"pieWord": pieWord, "languageCode": word.lang, "latinTranscription": word.latinTranscription, "meaning": word.meaning}}, upsert=True)
#                         except:
#                             continue

#                     pieWordJSON = {"pieWord": pieWord, "meaning": meaning, "descendants": cognates}
#                     #print(pieWordJSON)
#                     #time.sleep(5)


#                     try: #sometimes pymongo can't encode weird characters
#                         collection.update({"pieWord": pieWord}, {"$set": {"meaning": meaning, "descendants": cognates}}, upsert=True)
#                     except:
#                         continue
