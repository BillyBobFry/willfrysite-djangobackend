from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import json
import time, datetime
import pymongo
import array
from pymongo import MongoClient
import xml.etree.ElementTree as ET
import requests

client = MongoClient('localhost', 27017)
db = client.indoEuropean
collection = db["indoEuropeanCognates2"]

headers = {'content-type': 'text/xml'}

# Simply return all stations that have had their delays queried
def index(request):
  if request.POST.get("requestType","")=="getDistinctWords":
    #distinctWords = collection.distinct("pieWord", {"lang": {"$ne": "ine-pro"}}) 
    distinctWords = collection.aggregate([{"$group": {"_id": {"pieWord": "$pieWord"}, "count": {"$sum": 1}}}, {"$sort": {"count": -1}},{"$match": {"count": {"$gt": 1}}}])

    words = []
   
    for word in distinctWords:
      print(word)
      wordCursor = collection.find({"pieWord": word["_id"]["pieWord"], "lang": "ine-pro"})
      for meaning in wordCursor:
        words.append({"word": meaning["meaning"], "pieWord": meaning["pieWord"]})
    
    return JsonResponse({"words": words})

  else:  
    pieWord = request.POST.get("word","")
    cognates = []
    # pieWordCursor = collection.find({"pieWord": word, "lang": "ine-pro"})
    # for word in pieWordCursor:
    #   print(word)
    #   pieWord = word["pieWord"]

    cognateCursor = collection.find({"pieWord": pieWord},{"_id": 0, "meaning": 1, "lang": 1, "latinTranscription": 1}) 
    for cognate in cognateCursor:
      cognates.append(cognate)    

    return JsonResponse({"results": cognates})
