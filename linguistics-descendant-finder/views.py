from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import json
import time, datetime
import pymongo
import array
from pymongo import MongoClient
import xml.etree.ElementTree as ET
import requests
import re

client = MongoClient('localhost', 27017)
db = client.linguistics
collection = db["doublets"]

headers = {'content-type': 'text/xml'}


def index(request):
    requestBody = json.loads(request.body)
    requestType = requestBody['requestType']
    # print(requestType)
    if requestType == "getLinks":
        returnVar = []
        parentLang = requestBody['parentLang']
        childLang = requestBody['childLang']

        childLangWords = list(collection.find({"lang": childLang}))
        for word in childLangWords:
            # go all the way back to the root, trying to find the desired parent lang

            wordChain = [word]
            rootLang = word["rootLang"]
            currentParentLang = word["parentLang"]
            parentWord = list(collection.find({"_id": word["parentID"]}))
            if len(parentWord) > 0:
                parentWord = parentWord[0]
            else:
                continue
            wordChain.insert(0, parentWord)

            while currentParentLang != parentLang and currentParentLang != rootLang and currentParentLang != "NONE":
                # print(parentWord["latinTranscription"])
                parentWord = list(
                    collection.find({"_id": parentWord["parentID"]}))
                if len(parentWord) > 0:
                    parentWord = parentWord[0]
                else:
                    break
                # print(parentWord)
                # print(parentWord[0])
                # parentWord = parentWord[0]
                wordChain.insert(0, parentWord)
                currentParentLang = parentWord["parentLang"]
                if currentParentLang == parentLang:
                    parentWord = list(
                        collection.find({"_id": parentWord["parentID"]}))
                    if len(parentWord) > 0:
                        parentWord = parentWord[0]
                        wordChain.insert(0, parentWord)
                    else:
                        break
            itemExists = False
            i_n = wordChain[len(wordChain) - 1]["latinTranscription"]
            i_0 = wordChain[0]["latinTranscription"]
            for chain in returnVar:
                j_n = chain[len(chain) - 1]["latinTranscription"]
                j_0 = chain[0]["latinTranscription"]
                if j_n == i_n and j_0 == i_0:  # then chain already exists
                    itemExists = True
                    break

            if currentParentLang == parentLang and itemExists == False:
                returnVar.append(wordChain)

        for link in returnVar:
            for word in link:
                word["_id"] = str(word["_id"])
                word["parentID"] = str(word["parentID"])

        return JsonResponse({"results": returnVar})

    elif requestType == "getAllLanguages":
        returnVar = []
        returnVar = list(collection.distinct("lang"))
        return JsonResponse({"results": returnVar})
