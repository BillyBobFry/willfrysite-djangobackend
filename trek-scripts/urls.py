from django.urls import path

from . import views

urlpatterns = [
    path('get-term-by-character',
         views.getTermByCharacter, name='getTermByCharacter'),
    path('get-significant-terms-by-character',
         views.getSignificantTermsByCharacter, name='getSignificantTermsByCharacter'),
    path('', views.index, name='index'),
]
