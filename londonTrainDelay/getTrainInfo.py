from urllib import request, parse
import re
import json
import xml.etree.ElementTree as ET
import time
from datetime import datetime
import pymongo
import array
from pymongo import MongoClient
import requests

# instantiate mongodb variables
client = MongoClient('localhost', 27017)
db = client.londonTrainInfo
collection = db["londonTrainInfo"]

# instantiate variables needed for National Rail Darwin API
darwinToken = "a197af6f-083f-484a-aa1a-7229036cea08"
darwinStation = "KDB"
darwinUrl = "https://lite.realtime.nationalrail.co.uk/OpenLDBWS/ldb6.asmx"
dateFormat = '%H:%M'

headers = {'content-type': 'text/xml'}

# contacts darwin to get station's departure board, and updates mongodb with average delay
def updateStation(stationCode):
  now = datetime.now()
  today = now.strftime("%Y-%m-%d")
  thisMonth = now.strftime("%Y-%m")

  print(stationCode)
  body = """<?xml version="1.0"?>
  <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://thalesgroup.com/RTTI/2014-02-20/ldb/" xmlns:ns2="http://thalesgroup.com/RTTI/2010-11-01/ldb/commontypes">
    <SOAP-ENV:Header>
      <ns2:AccessToken>
        <ns2:TokenValue>""" + darwinToken + """</ns2:TokenValue>
      </ns2:AccessToken>
    </SOAP-ENV:Header>
    <SOAP-ENV:Body>
      <ns1:GetDepartureBoardRequest>
        <ns1:numRows>10</ns1:numRows>
        <ns1:crs>""" + stationCode + """</ns1:crs>
      </ns1:GetDepartureBoardRequest>
    </SOAP-ENV:Body>
  </SOAP-ENV:Envelope>"""
  response = requests.post(darwinUrl,data=body,headers=headers)

  # update the count of requests sent to Darwin, to make sure we don't go over the 5,000,000 per month limit
  db["apiCallCount"].update({"_id": today}, {"$inc": {"count": 1}}, upsert=True)
  db["apiCallCount"].update({"_id": thisMonth}, {"$inc": {"count": 1}}, upsert=True)


  
  tree = ET.fromstring(response.content)

  delays = []
  trainServices = tree[0][0][0].find('{http://thalesgroup.com/RTTI/2014-02-20/ldb/types}trainServices')
  try:
    for service in trainServices:
      scheduled = service.find('{http://thalesgroup.com/RTTI/2014-02-20/ldb/types}std').text
      expected = service.find('{http://thalesgroup.com/RTTI/2014-02-20/ldb/types}etd').text
      if expected == "On time":
        diff = 0
      elif expected == "Cancelled" or expected == "Delayed":
        diff = 20
        continue
      else:
        t0 = scheduled
        t1 = expected
        tdelta = datetime.strptime(t1, dateFormat) - datetime.strptime(t0, dateFormat)
        diff = tdelta.total_seconds()/60
      
      delays.append(diff)

    if len(delays) == 0: # if there are no departures on the board, then the station looks pretty screwed.
      averageDelay = 9999
    else:
      averageDelay = sum(delays)/len(delays)
    stationDict = {"_id": stationCode, "latestDelay": averageDelay, "lastUpdated": now}
    collection.update({"_id": stationCode}, {"$set": stationDict}, upsert=True)
  except Exception as e:
    print(e)
    print("Couldn't update station " + stationCode)
  
# returns an array of all stations within a given bounding box
def getStationsInBox(lat_a, lat_b, lng_a, lng_b):
  return(list(collection.find({"Latitude": {"$gt": lat_a, "$lt": lat_b}, "Longitude": {"$gt": lng_a, "$lt": lng_b}}, {"_id": 1})))	

# -1.148071,51.162122,0.937958,51.779736
for station in getStationsInBox(51.162122, 51.779736, -1.148071, 0.937958):
  updateStation(station["_id"])
