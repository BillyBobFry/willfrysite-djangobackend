import csv
import json
import pandas as pd
import sys, getopt, pprint
from pymongo import MongoClient
# run this to build the collection of train stations
csvfile = open('/var/www/djangoApps/londonTrainDelay/UKstations.csv', 'r')
reader = csv.DictReader( csvfile )
client = MongoClient('localhost', 27017)
db = client.londonTrainInfo
collection = db["londonTrainInfo"]
# define the columns we want to capture
header= [ "Station", "Latitude","Longitude","TLC","NLC","Owner","SRS","Entries and exits 2018"]

for each in reader:
    row={}
    for field in header:
        # we make the TLC the ID so that we can replace/insert the document when calling getTrainInfo.py
        if field == "TLC":
            row["_id"] = each[field]
        elif field == "Latitude" or field == "Longitude" or field =="Entries and exits 2018":
            row[field] = float(each[field]) # store these columns as numbers
        else:
            row[field]=each[field] # otherwise store as a string

    collection.insert(row)