from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import json
import time, datetime
import pymongo
import array
from pymongo import MongoClient
import xml.etree.ElementTree as ET
import requests

client = MongoClient('localhost', 27017)
db = client.londonTrainInfo
collection = db["londonTrainInfo"]

# instantiate variables needed for National Rail Darwin API
darwinToken = "a197af6f-083f-484a-aa1a-7229036cea08"
darwinStation = "KDB"
darwinUrl = "https://lite.realtime.nationalrail.co.uk/OpenLDBWS/ldb6.asmx"
dateFormat = '%H:%M'

headers = {'content-type': 'text/xml'}


# Simply return all stations that have had their delays queried
def index(request):
    print(request)
    print(request.body)
    requestBody = json.loads(request.body)
    print(requestBody)
    lat = requestBody['lat']
    lng = requestBody['lng']
    print(lat)
    print(lng)

    # get the current number of API calls sent today (so that we can stick within 5,000,000 per month)
    today = datetime.datetime.now().strftime("%Y-%m-%d")
    apiCalls = list(db["apiCallCount"].find({"_id": today}))
    if apiCalls:
        callsToday = list(db["apiCallCount"].find({"_id": today}))[0]["count"]
    else:
        callsToday = 0

    # only send call if we're not in danger of exceeding the 5,000,000 per month limit
    # (we're allowed ~166,000 per day, and give some extra room for the scheduled update of London stations)
    if callsToday < 140000:
        # define time bounds for "stale" stations
        now = datetime.datetime.now()
        timeLimit = now - datetime.timedelta(hours=1)

        # get all stations in the current bounds that haven't been updated in time period
        staleStations = list(
            collection.find({
                "Latitude": {
                    "$gt": float(lat[0]),
                    "$lt": float(lat[1])
                },
                "Longitude": {
                    "$gt": float(lng[0]),
                    "$lt": float(lng[1])
                },
                "$or": [{
                    "lastUpdated": {
                        "$lt": timeLimit
                    }
                }, {
                    "lastUpdated": None
                }]
            }).sort([("Entries and exits 2018", pymongo.DESCENDING)]).limit(6))

        # for all of those stations, update the departure board
        for station in staleStations:
            updateStation(station["_id"])

        stations = list(
            collection.find(
                {
                    "latestDelay": {
                        "$exists": "true"
                    },
                    "lastUpdated": {
                        "$gt": timeLimit
                    }
                }, {
                    "Station": 1,
                    "Latitude": 1,
                    "Longitude": 1,
                    "latestDelay": 1,
                    "lastUpdated": 1,
                    "_id": 1
                }))

    return JsonResponse({
        "stations": stations,
        "updatedStations": len(staleStations)
    })


# contacts darwin to get station's departure board, and updates mongodb with average delay
def updateStation(stationCode):
    now = datetime.datetime.now()
    today = now.strftime("%Y-%m-%d")
    thisMonth = now.strftime("%Y-%m")

    print(stationCode)
    body = """<?xml version="1.0"?>
  <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://thalesgroup.com/RTTI/2014-02-20/ldb/" xmlns:ns2="http://thalesgroup.com/RTTI/2010-11-01/ldb/commontypes">
    <SOAP-ENV:Header>
      <ns2:AccessToken>
        <ns2:TokenValue>""" + darwinToken + """</ns2:TokenValue>
      </ns2:AccessToken>
    </SOAP-ENV:Header>
    <SOAP-ENV:Body>
      <ns1:GetDepartureBoardRequest>
        <ns1:numRows>10</ns1:numRows>
        <ns1:crs>""" + stationCode + """</ns1:crs>
      </ns1:GetDepartureBoardRequest>
    </SOAP-ENV:Body>
  </SOAP-ENV:Envelope>"""
    response = requests.post(darwinUrl, data=body, headers=headers)
    tree = ET.fromstring(response.content)

    # update the count of requests sent to Darwin, to make sure we don't go over the 5,000,000 per month limit
    db["apiCallCount"].update({"_id": today}, {
        "$set": {
            "_id": today
        },
        "$inc": {
            "count": 1
        }
    },
                              upsert=True)
    db["apiCallCount"].update({"_id": thisMonth}, {
        "$set": {
            "_id": thisMonth
        },
        "$inc": {
            "count": 1
        }
    },
                              upsert=True)

    delays = []
    trainServices = tree[0][0][0].find(
        '{http://thalesgroup.com/RTTI/2014-02-20/ldb/types}trainServices')
    try:
        for service in trainServices:
            scheduled = service.find(
                '{http://thalesgroup.com/RTTI/2014-02-20/ldb/types}std').text
            expected = service.find(
                '{http://thalesgroup.com/RTTI/2014-02-20/ldb/types}etd').text
            if expected == "On time":
                diff = 0
            elif expected == "Cancelled" or expected == "Delayed":
                diff = 20
                continue
            else:
                t0 = scheduled
                t1 = expected
                tdelta = datetime.datetime.strptime(
                    t1, dateFormat) - datetime.datetime.strptime(
                        t0, dateFormat)
                diff = tdelta.total_seconds() / 60

            delays.append(diff)

        if len(
                delays
        ) == 0:  # if there are no departures on the board, then the station looks pretty screwed.
            averageDelay = 9999
        else:
            averageDelay = sum(delays) / len(delays)
        stationDict = {
            "_id": stationCode,
            "latestDelay": averageDelay,
            "lastUpdated": datetime.datetime.now()
        }
        collection.update({"_id": stationCode}, {"$set": stationDict},
                          upsert=True)
    except Exception as e:
        print(e)
        print("Couldn't update station " + stationCode)
        collection.update({"_id": stationCode},
                          {"$set": {
                              "lastUpdated": datetime.datetime.now()
                          }},
                          upsert=True)
