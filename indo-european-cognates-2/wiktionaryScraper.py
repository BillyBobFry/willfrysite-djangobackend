import requests
from bs4 import BeautifulSoup, Tag, NavigableString
import re
import json
import time, datetime
import pymongo
import array
from pymongo import MongoClient
import random

# set mongo vars
client = MongoClient('localhost', 27017)
db = client.indoEuropean
collection = db["indoEuropeanCognates2"]
outfile=open("output.txt","a+", encoding="utf-8")
outfile.write("Test")
#set urls
rootURL = 'https://en.wiktionary.org'
pieNounsURL = 'https://en.m.wiktionary.org/wiki/Appendix:List_of_Proto-Indo-European_nouns'
pieLangCode = "ine-pro"
def parseWiktionaryPage(wiktionaryURL, inputLanguageCode, inputWord, pieWord, inputMeaning, derivedTerm):
    print("Derived root:")
    print(derivedTerm)
    print("Input word:")
    print(inputWord)
    outputMeaning = inputMeaning
    request_object = requests.get(rootURL + wiktionaryURL)
    searchResponse = BeautifulSoup(request_object.content, 'html.parser')
    
    def parseDescendantList(descendants):
        for descendant in descendants:
            # if the word isn't borrowed, process
            if descendant.find("span", attrs={"title": ["borrowed", "derived by addition of morphemes"]}, recursive=False) == None:
                descendant_a = descendant.find("a")
                if descendant_a:
                    descendantLink = descendant_a.get("href")

                    try:
                        # print(descendant_a.parent.get("class"))
                        # print(descendant_a.parent.get("class")==["Latn"])
                        if descendant_a.parent.get("class")==["Latn"] or descendant_a.parent.get("class")==["Latinx"]:
                            descendantWord = descendant_a.contents[0]
                        else:
                            descendantWord = descendant.find(attrs={"class": ["Latinx", "Latn"]}).contents[0]
                        # print("DescendantWord: " + descendantWord)
                        if descendant.find(attrs={"class": ["Latn", "Latinx"]}):
                            descendantLang = descendant.find(attrs={"class": ["Latn", "Latinx"]}).get("lang")
                            print("parsing page " + descendantLink + " for word " + descendantWord + " in lang " + descendantLang + " (" + pieWord + ").")
                            parseWiktionaryPage(descendantLink, descendantLang, descendantWord, pieWord, outputMeaning, None)
                    except:
                        pass

    def parseDerivedTerms(derivedTerms):
        # print("------------------------------------------------------")
        # print("DERIVED TERMS")
        # print("------------------------------------------------------")
        for term in derivedTerms:
            derivedRoot = term.find(attrs={"lang": inputLanguageCode})

            # print(str(term.contents[0].encode("utf-8"))[:22])
            # unsorted = (str(term.contents[0].encode("utf-8"))[:22]=="b'Unsorted formations:")
            if not derivedRoot:
                derivedRoot = term
                derivedWord = "derived"
            else:
                derivedWord = derivedRoot.contents[0]

            childList = derivedRoot.find_next("ul").find_all("li", recursive=False)

            for child in childList:
                childElement = child.find(attrs={"class":"Latinx"}, recursive=False)
                if childElement:
                    childLang = childElement.get("lang")
                    if childElement.find("a"):
                        childLink = childElement.find("a").get("href")

                        # work out if we need to parse this page
                        wordExists = collection.find({"lang": childLang, "pieWord": pieWord}).count()
                        print(wordExists)
                        if wordExists==0:
                            childWord = childElement.find("a").contents[0]
                            parseWiktionaryPage(childLink, childLang, childWord, pieWord, inputMeaning, derivedWord)


        return

    # first find transliterated headword in the right lang
    headword = searchResponse.find(attrs={"class": "headword-tr", "lang": inputLanguageCode})
    if headword is None: #, then the word probably naturally appears in latin script
        print("no transliteration found")
        # find headword in latin script
        headword = searchResponse.find(attrs={"class": ["headword"], "lang": inputLanguageCode})

    # should always be true, so probably don't need this
    if headword:       
        outputWord = headword.contents[0] # set the word we're inserting (its latin transcription)
        print("Word to insert: " + outputWord)
        # let's try and find the meaning
        if headword.find_next("li").find("a"):
            outputMeaning = headword.find_next("li").find("a").contents[0]
        else: # if we can't find a new meaning, assume the meaning hasn't changed
            outputMeaning = inputMeaning

        # if we're on PIE, pieWord is the headword
        if inputLanguageCode == "ine-pro":
            pieWord = outputWord

        # find list of descendants 
        # first if there's a next headword (i.e. other languages on that page)
        if headword.find_next(attrs={"class": ["headword", "headword-tr"]}):
            # only process descendants if they belong to the correct language
            if headword.find_next(attrs={"id":"Descendants"}) != headword.find_next(attrs={"class": ["headword", "headword-tr"]}).find_next(attrs={"id":"Descendants"}):
                descendantHeader = headword.find_next(attrs={"id":"Descendants"})
                descendantList = descendantHeader.find_next("ul").find_all("li")
                parseDescendantList(descendantList)
            # only process derived terms if they belong to the correct language
            if headword.find_next(attrs={"id":"Derived_terms"}) != headword.find_next(attrs={"class": ["headword", "headword-tr"]}).find_next(attrs={"id":"Derived_terms"}) and inputLanguageCode==pieLangCode:
                derivedTermsHeader = headword.find_next(attrs={"id":"Derived_terms"})
                # print("------------------------------------------------------")
                # print("DERIVED TERMS HEADER")
                # print("------------------------------------------------------")
                print(derivedTermsHeader)
                if derivedTermsHeader:
                    derivedTermsList = derivedTermsHeader.find_next("ul").find_all("li", recursive=False)
                    parseDerivedTerms(derivedTermsList)

       # if there are no more languages on that page, things are easier
        elif headword.find_next(attrs={"class": ["headword", "headword-tr"]}) is None:
            descendantHeader = headword.find_next(attrs={"id":"Descendants"})
            if descendantHeader:
                descendantList = descendantHeader.find_next("ul").find_all("li")
                parseDescendantList(descendantList)
            derivedTermsHeader = headword.find_next(attrs={"id":"Derived_terms"})
                # print("------------------------------------------------------")
                # print("DERIVED TERMS HEADER")
                # print("------------------------------------------------------")
                # print(derivedTermsHeader)
            if (derivedTermsHeader and inputLanguageCode==pieLangCode):
                derivedTermsList = derivedTermsHeader.find_next("ul").find_all("li", recursive=False)
                parseDerivedTerms(derivedTermsList)
    
    else:
        outputWord = inputWord
    # find IPA Transcription of headword
    try:
        if headword.find_previous(attrs={"class": "IPA"}) != headword.find_previous(attrs={"class": ["headword", "headword-tr"]}).find_previous(attrs={"class": "IPA"}):
            ipaTranscription = headword.find_previous(attrs={"class": "IPA"}).find_parent("ul").find(attrs={"class": "IPA"}).contents[0]
            #print(ipaTranscription)
        else:
            ipaTranscription = None
    except:
        ipaTranscription = None
        pass

    try:
        collection.insert({"lang": inputLanguageCode, "latinTranscription": outputWord, "ipaTranscription": ipaTranscription, "pieWord": pieWord, "meaning": outputMeaning, "url": wiktionaryURL, "derivedRoot": derivedTerm, "tags": [], "soundChanges": []})
        print("inserted " + outputWord + " for " + inputLanguageCode)
    except:
        pass

    searchResponse.decompose()
    return

lemmaURL = "https://en.wiktionary.org/w/index.php?title=Category:Proto-Indo-European_lemmas&from=sl" 
#lemmaURL = "https://en.wiktionary.org/wiki/Reconstruction:Proto-Indo-Iranian/d%CA%B0%C3%A1%C7%B0%CA%B0ati"
lemma_request_object = requests.get(lemmaURL)
lemma_searchResponse = BeautifulSoup(lemma_request_object.content, 'html.parser')

# parseWiktionaryPage("/wiki/Reconstruction:Proto-Indo-European/%C7%B5%C3%B3nu", "ine-pro", None, None, None, None)
# print("inserted test word")


# time.sleep(20)
# print(lemma_searchResponse)
#print(lemma_searchResponse.find("div", attrs={"class": "mw-content-ltr"}))
lemmas = lemma_searchResponse.find(attrs={"id": "mw-pages"}).find_all("li")
for lemma in lemmas:
    print(lemma.find("a").contents[0])
    lemmaHref = lemma.find("a").get("href")
    lemmaURL = lemma.find("a").contents[0]
    parseWiktionaryPage(lemmaHref, "ine-pro", None, None, None, None)

print("DONE WITH INSERT, CHECK IT")



