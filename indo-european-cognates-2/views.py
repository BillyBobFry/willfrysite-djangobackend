from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import json
import time, datetime
import pymongo
import array
from pymongo import MongoClient
import xml.etree.ElementTree as ET
import requests
import re

client = MongoClient('localhost', 27017)
db = client.linguistics
collection = db["etymology"]

headers = {'content-type': 'text/xml'}


# Simply return all stations that have had their delays queried
def index(request):
    requestBody = json.loads(request.body)
    requestType = requestBody['requestType']
    print(requestType)
    if requestType=="getDistinctWords":
        print("needed?")
        #distinctWords = collection.distinct("pieWord", {"lang": {"$ne": "ine-pro"}})
        # distinctWords = collection.aggregate([{"$match": {"lang": {"$ne": "ine-pro"}}},{"$group": {"_id": {"pieWord": "$pieWord"}, "count": {"$sum": 1}}}, {"$sort": {"count": -1}}])

        # words = []

        # for word in distinctWords:
        #   wordCursor = collection.find({"pieWord": word["_id"]["pieWord"], "latinTranscription": word["_id"]["pieWord"], "lang": "ine-pro"})
        #   for meaning in wordCursor:
        #     words.append({"word": meaning["meaning"], "pieWord": meaning["pieWord"]})

        # return JsonResponse({"results": words})

    elif requestType=="getAllLanguages":
        distinctLanguages = db["languages"].find()
        returnArray = []
        for lang in distinctLanguages:
            returnArray.append({"code": lang["code"], "name": lang["name"]})

        return JsonResponse({"results": returnArray})

    elif requestType=="getAllRootLangs":
        print("HERE")
        distinctLanguages = db["etymology"].distinct("rootLang")
        print(distinctLanguages)
        return JsonResponse({"results": distinctLanguages})

    elif requestType=="getDistinctWords_tag":
        rootLang = request.POST.get("rootLang","")
        returnRoots = request.POST.get("returnRoots","")
        tag = request.POST.get("tag","")
        # distinctWords = collection.aggregate([{"$match": {"rootLang": rootLang, "tags": {"name": tag, "val": True}}},{"$group": {"_id": {"rootLatinTranscription": "$rootLatinTranscription"}, "count": {"$sum": 1}}}, {"$sort": {"count": -1}}])
        words = []

        if returnRoots=="true":
            distinctWords = collection.aggregate([{"$match": { "rootLang": rootLang, "tags": {"name": tag, "val": True}}},{"$group": {"_id": {"rootLatinTranscription": "$rootLatinTranscription"}}}, {"$sort": {"_id": 1}}])
        else:
            regx = re.compile('.*[^-]$', re.IGNORECASE)
            distinctWords = collection.aggregate([{"$match": { "rootLang": rootLang, "rootLatinTranscription": regx, "tags": {"name": tag, "val": True}}},{"$group": {"_id": {"rootLatinTranscription": "$rootLatinTranscription"}}}, {"$sort": {"_id": 1}}])

        for word in distinctWords:
            wordCursor = collection.find({"rootLatinTranscription": word["_id"]["rootLatinTranscription"], "latinTranscription": word["_id"]["rootLatinTranscription"], "lang": rootLang})
            for meaning in wordCursor:
                words.append({"word": meaning["meaning"], "rootLatinTranscription": meaning["rootLatinTranscription"]})

        return JsonResponse({"results": words})

    elif requestType=="getDistinctWords_soundChange":
        soundChange = request.POST.get("soundChange","")
        returnRoots = request.POST.get("returnRoots","")
        rootLang = request.POST.get("rootLang","")

        if returnRoots=="true":
            distinctWords = collection.aggregate([{"$match": {"rootLang": rootLang, "soundChanges": {"name": soundChange, "val": True}}},{"$group": {"_id": {"rootLatinTranscription": "$rootLatinTranscription"}}}, {"$sort": {"_id": 1}}])
        else:
            regx = re.compile('.*[^-]$', re.IGNORECASE)
            distinctWords = collection.aggregate([{"$match": { "rootLang": rootLang, "rootLatinTranscription": regx, "soundChanges": {"name": soundChange, "val": True}}},{"$group": {"_id": {"rootLatinTranscription": "$rootLatinTranscription"}}}, {"$sort": {"_id": 1}}])


        words = []
        for word in distinctWords:
            wordCursor = collection.find({"rootLatinTranscription": word["_id"]["rootLatinTranscription"], "latinTranscription": word["_id"]["rootLatinTranscription"], "lang": rootLang})
            for meaning in wordCursor:
                words.append({"word": meaning["meaning"], "rootLatinTranscription": meaning["rootLatinTranscription"], "soundChanges": meaning["soundChanges"], "tags": meaning["tags"]})

        return JsonResponse({"results": words})


    elif requestType=="submitWord":
        # check to see if request contains valid token
        suppliedCode = request.POST.get("adminCode","")
        validCode = db["adminCodes"].count({"code": suppliedCode})
        # validCode = True
        if validCode:
            oldWord = json.loads(request.POST.get("originalWord",""))
            newWord = json.loads(request.POST.get("wordToSubmit",""))
            if oldWord:
                collection.update({"lang": oldWord["lang"], "parentLang": oldWord["parentLang"], "latinTranscription": oldWord["latinTranscription"], "parentLatinTranscription": oldWord["parentLatinTranscription"],  "rootLatinTranscription": oldWord["rootLatinTranscription"]}, {"$set": {"lang": newWord["lang"], "parentLang": newWord["parentLang"], "meaning": newWord["meaning"], "ipaTranscription": newWord["ipaTranscription"], "latinTranscription": newWord["latinTranscription"], "parentLatinTranscription": newWord["parentLatinTranscription"], "soundChanges": newWord["soundChanges"], "isBorrowed": newWord["isBorrowed"], "tags": newWord["tags"]}}, multi=True)
            else:
                collection.insert({"lang": newWord["lang"], "parentLang": newWord["parentLang"], "rootLang": newWord["rootLang"], "ipaTranscription": newWord["ipaTranscription"], "latinTranscription": newWord["latinTranscription"], "parentLatinTranscription": newWord["parentLatinTranscription"], "rootLatinTranscription": newWord["rootLatinTranscription"], "soundChanges": newWord["soundChanges"], "isBorrowed": newWord["isBorrowed"], "tags": newWord["tags"]})

            # log the action
            user = db["adminCodes"].find_one({"code": suppliedCode})
            db["adminLog"].insert({"user": user["user"], "oldWord": oldWord, "newWord": newWord})

            return JsonResponse({"success": True})
        else:
            return JsonResponse({"success": False})

    elif requestType=="deleteWord":
        # check to see if request contains valid token
        suppliedCode = request.POST.get("adminCode","")
        validCode = db["adminCodes"].count({"code": suppliedCode})
        if validCode:
            wordToDelete = json.loads(request.POST.get("wordToDelete",""))
            collection.remove({"lang": wordToDelete["lang"], "parentLang": wordToDelete["parentLang"], "latinTranscription": wordToDelete["latinTranscription"], "parentLatinTranscription": wordToDelete["parentLatinTranscription"], "rootLatinTranscription": wordToDelete["rootLatinTranscription"]})

            return JsonResponse({"success": True})
        else:
            return JsonResponse({"success": False})

    elif requestType=="getAllWordsInLanguage":
        lang = request.POST.get("lang", "")
        wordCursor = collection.find({"lang": lang},{"_id": 0, "lang": 1, "latinTranscription": 1, "rootLatinTranscription": 1, "meaning": 1, "rootLang": 1})
        words = []
        for word in wordCursor:
            words.append(word)

        return JsonResponse({"results": words})

    elif requestType=="countTags":
        rootLang = request.POST.get("rootLang", "")
        returnRoots = request.POST.get("returnRoots", "")

        if returnRoots=="true":
            distinctWords = collection.find({"lang": rootLang})
        else:
            regx = re.compile('^.[^-].*[^-]$', re.IGNORECASE)
            # distinctWords = collection.aggregate([{"$match": { "rootLang": rootLang, "rootLatinTranscription": regx, "lang": {"$ne": rootLang}}},{"$group": {"_id": {"rootLatinTranscription": "$rootLatinTranscription"}, "count": {"$sum": 1}}}, {"$sort": {"count": -1}}])
            distinctWords = collection.find({"lang": rootLang, "rootLatinTranscription": regx})

        # instantiate tag count object
        tagCount = {}

        for word in distinctWords: #for each word
            for tag in word["tags"]: # loop through each tag
                try:
                    thisTag = tag["name"] # get the name of the tag

                    # if val is true
                    if tag["val"]==True:
                        # if the tag exists in tagCount, increment
                        if thisTag in tagCount:
                            tagCount[thisTag] += 1
                        else: # else, create with 1
                            tagCount[thisTag] = 1
                except:
                    pass

        return JsonResponse({"results": tagCount})

    elif requestType==" oundChanges":
        rootLang = request.POST.get("rootLang", "")
        returnRoots = request.POST.get("returnRoots", "")

        if returnRoots=="true":
            distinctWords = collection.find({"rootLang": rootLang})
        else:
            regx = re.compile('^.[^-].*[^-]$', re.IGNORECASE)
            # distinctWords = collection.aggregate([{"$match": { "rootLang": rootLang, "rootLatinTranscription": regx, "lang": {"$ne": rootLang}}},{"$group": {"_id": {"rootLatinTranscription": "$rootLatinTranscription"}, "count": {"$sum": 1}}}, {"$sort": {"count": -1}}])
            distinctWords = collection.find({"rootLang": rootLang, "rootLatinTranscription": regx})

        # instantiate sound change count object
        soundChangeCount = {}
        soundChangeRootTracker = []

        for word in distinctWords: #for each word
            for soundChange in word["soundChanges"]: # loop through each tag
                try:
                    thisSoundChange = soundChange["name"] # get the name of the tag

                    # if val is true
                    if soundChange["val"]==True:

                        # only increment if sound change has not already been processed for this root
                        if word["rootLatinTranscription"] not in soundChangeRootTracker:
                            soundChangeRootTracker.append(word["rootLatinTranscription"])
                            # if the soundChange exists in soundChangeCount, increment
                            if thisSoundChange in soundChangeCount:
                                soundChangeCount[thisSoundChange] += 1
                            else: # else, create with 1
                                soundChangeCount[thisSoundChange] = 1

                except:
                    pass

        return JsonResponse({"results": soundChangeCount})

    elif requestType=="getAllChildLangs":
        rootLang = request.POST.get("rootLang","")
        # distinctWords = collection.aggregate([{"$match": {"tags": {"name": tag, "val": True}}},{"$group": {"_id": {"pieWord": "$pieWord"}, "count": {"$sum": 1}}}, {"$sort": {"count": -1}}])

        # descendantCursor = db["etymology"].aggregate([{"$match": {"rootLang": rootLang}}, {"$group": {"_id": {"rootLang": "$rootLang"}}}])


    elif requestType=="getWord":
        rootWord = requestBody["word"]
        cognates = []


        cognateCursor = db["etymology"].find({"rootLatinTranscription": rootWord},{"_id": 0, "meaning": 1, "lang": 1, "parentLang": 1, "rootLatinTranscription": 1, "parentLatinTranscription": 1, "latinTranscription": 1, "ipaTranscription": 1, "tags": 1, "soundChanges": 1, "derivedRoot": 1, "isBorrowed": 1, "isDerived": 1, "url": 1})
        for cognate in cognateCursor:
            cognates.append(cognate)

        return JsonResponse({"results": cognates})

    elif requestType=="getDistinctRoots":
        rootLang = requestBody['rootLang']
        returnRoots = requestBody['returnRoots']

        if returnRoots:
            distinctWords = db["etymology"].aggregate([{"$match": { "rootLang": rootLang, "lang": {"$ne": rootLang}}},{"$group": {"_id": {"rootLatinTranscription": "$rootLatinTranscription"}}}, {"$sort": { "_id": 1 }} ])
        else:
            regx = re.compile('^.[^-].*[^-]$', re.IGNORECASE)
            distinctWords = db["etymology"].aggregate([{"$match": { "rootLang": rootLang, "rootLatinTranscription": regx, "lang": {"$ne": rootLang}}},{"$group": {"_id": {"rootLatinTranscription": "$rootLatinTranscription"}}}, {"$sort": { "_id": 1 }}])

        words = []

        for word in distinctWords:
            wordCursor = db["etymology"].find({"rootLatinTranscription": word["_id"]["rootLatinTranscription"], "latinTranscription": word["_id"]["rootLatinTranscription"], "lang": rootLang})
            for meaning in wordCursor:
                words.append({"word": meaning["meaning"], "rootLatinTranscription": meaning["rootLatinTranscription"]})

        return JsonResponse({"results": words, "eh?": "eh"})

    else:
        print("else")
        # print(requestType)
        # print(request.POST)
        # print(json.loads(request.POST))

        rootWord = request.POST.get("word","")
        cognates = []
        # pieWordCursor = collection.find({"pieWord": word, "lang": "ine-pro"})
        # for word in pieWordCursor:
        #   print(word)
        #   pieWord = word["pieWord"]

        cognateCursor = collection.find({"rootLatinTranscription": rootWord},{"_id": 0, "meaning": 1, "lang": 1, "latinTranscription": 1, "ipaTranscription": 1, "tags": 1, "soundChanges": 1, "derivedRoot": 1})
        for cognate in cognateCursor:
            cognates.append(cognate)

        return JsonResponse({"results": cognates})
