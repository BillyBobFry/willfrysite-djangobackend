import requests
from bs4 import BeautifulSoup, Tag, NavigableString
import re
import json
import time, datetime
import pymongo
import array
from pymongo import MongoClient
import random

# set mongo vars
client = MongoClient('localhost', 27017)
db = client.linguistics
collection = db["etymology"]
outfile=open("output.txt","a+", encoding="utf-8")
outfile.write("Test")
#set urls

import linecache
import sys

def PrintException():
    exc_type, exc_obj, tb = sys.exc_info()
    f = tb.tb_frame
    lineno = tb.tb_lineno
    filename = f.f_code.co_filename
    linecache.checkcache(filename)
    line = linecache.getline(filename, lineno, f.f_globals)
    print('EXCEPTION IN ({}, LINE {} "{}"): {}'.format(filename, lineno, line.strip(), exc_obj))


rootURL = 'https://en.wiktionary.org'
pieNounsURL = 'https://en.m.wiktionary.org/wiki/Appendix:List_of_Proto-Indo-European_nouns'
iteration=0
def parseWiktionaryPage(wiktionaryURL, rootLanguageCode, parentLanguageCode, inputLanguageCode, rootWord, parentWord, inputWord, inputMeaning, derivedTerm, isBorrowed):
    time.sleep(0.25)
    global iteration
    iteration = iteration + 1 

    # if inputLanguageCode=="la":
    #     print("pausing for debug")

    if iteration > 1000:
        print("REACHED MAXIMUM ITERATIONS. SAY GOODBYE.")
        return
    ipaTranscription = None
    print(wiktionaryURL)
    if wiktionaryURL[0:6] != "/wiki/" and wiktionaryURL[0:3] != "/w/":
        return

    # print("Derived root: " + str(derivedTerm))
    # print("Input word:" + str(inputWord))
    outputMeaning = inputMeaning
    request_object = requests.get(rootURL + wiktionaryURL)
    searchResponse = BeautifulSoup(request_object.content, 'html.parser')
    # print(searchResponse)

    def parseDerivedTerms(derivedTerms, parentWord):
        # print("------------------------------------------------------")
        # print("DERIVED TERMS")
        # print("------------------------------------------------------")
        iter = 0
        
        for term in derivedTerms:
            outputWord = inputWord
            # iter = iter+1
            semirandom_string = str(int(time.time()*10000000))[10:]
            # print(type(term))
            if isinstance(term, Tag):
                derivedRoot = term.find(attrs={"lang": inputLanguageCode})

                # print(str(term.contents[0].encode("utf-8"))[:22])
                # unsorted = (str(term.contents[0].encode("utf-8"))[:22]=="b'Unsorted formations:")
                if not derivedRoot:
                    if term.contents:
                        derivedWord = term.contents[0] if term.get("lang")==inputLanguageCode or str(term.contents[0].encode("utf-8"))[:22]=="b'Unsorted formations:" else "derived"              
                        while isinstance(derivedWord, Tag):
                            derivedWord = derivedWord.contents[0]

                        # shall we try and parse the link?
                        derived_link = term.find("a")
                        if derived_link:
                            if derived_link.getText() == term.getText():
                                derived_link_url = derived_link.get("href")
                                parseWiktionaryPage(wiktionaryURL=derived_link_url, rootLanguageCode=rootLanguageCode, parentLanguageCode=(inputLanguageCode + semirandom_string), inputLanguageCode=inputLanguageCode, parentWord="placeholder", rootWord=rootWord, inputWord=derivedWord, inputMeaning=inputMeaning, derivedTerm=None, isBorrowed=False)


                        if derivedWord != rootWord: #sometimes the derived word is the word itself. This causes duplicate keys, and is dumb - why are they not just descendants?
                            try:
                                collection.insert({"lang": inputLanguageCode + semirandom_string, "parentLang": inputLanguageCode, "rootLang": rootLanguageCode, "latinTranscription": "placeholder", "parentLatinTranscription":  parentWord,  "rootLatinTranscription": rootWord, "ipaTranscription": None, "meaning": inputMeaning, "url": wiktionaryURL, "derivedRoot": None, "isBorrowed": False, "tags": [], "soundChanges": []})
                            except pymongo.errors.DuplicateKeyError:
                                pass
                            try:    
                                collection.insert({"lang": inputLanguageCode, "parentLang": inputLanguageCode + semirandom_string, "rootLang": rootLanguageCode, "latinTranscription": derivedWord, "parentLatinTranscription":  "placeholder",  "rootLatinTranscription": rootWord, "ipaTranscription": None, "meaning": inputMeaning, "url": wiktionaryURL, "derivedRoot": None, "isBorrowed": False, "isDerived": True, "tags": [], "soundChanges": []})
                            except pymongo.errors.DuplicateKeyError:
                                pass
                            
                            outputWord = derivedWord
                            # print("inserted " + outputWord + " for " + inputLanguageCode)
                        else:
                            outputWord = derivedWord
                        
                        derivedRoot = term
                    else:
                        continue

                else:
                    derivedWord = derivedRoot.getText()
                    while isinstance(derivedWord, Tag):
                        derivedWord = derivedWord.contents[0]

                    # shall we try and parse the link?
                    derived_link = derivedRoot.find("a")
                    if derived_link:
                        if derived_link.getText() == derivedRoot.getText():
                            derived_link_url = derived_link.get("href")
                            parseWiktionaryPage(wiktionaryURL=derived_link_url, rootLanguageCode=rootLanguageCode, parentLanguageCode=(inputLanguageCode + semirandom_string), inputLanguageCode=inputLanguageCode, parentWord="placeholder", rootWord=rootWord, inputWord=derivedWord, inputMeaning=inputMeaning, derivedTerm=None, isBorrowed=False)
                    
                    outputWord = derivedWord


                    if derivedWord != rootWord: #sometimes the derived word is the word itself. This causes duplicate keys, and is dumb - why are they not just descendants?
                        try:
                            collection.insert({"lang": inputLanguageCode + semirandom_string, "parentLang": inputLanguageCode, "rootLang": rootLanguageCode, "latinTranscription": "placeholder", "parentLatinTranscription":  parentWord,  "rootLatinTranscription": rootWord, "ipaTranscription": None, "meaning": inputMeaning, "url": wiktionaryURL, "derivedRoot": None, "isBorrowed": False, "tags": [], "soundChanges": []})
                        except pymongo.errors.DuplicateKeyError:
                            pass
                        
                        try:
                            collection.insert({"lang": inputLanguageCode, "parentLang": inputLanguageCode + semirandom_string, "rootLang": rootLanguageCode, "latinTranscription": derivedWord, "parentLatinTranscription":  "placeholder",  "rootLatinTranscription": rootWord, "ipaTranscription": None, "meaning": inputMeaning, "url": wiktionaryURL, "derivedRoot": None, "isBorrowed": False, "isDerived": True, "tags": [], "soundChanges": []})
                        except pymongo.errors.DuplicateKeyError:
                            pass
                        
                        outputWord = derivedWord
                        # print("inserted " + outputWord + " for " + inputLanguageCode)
                        # print(e)



                # if derivedRoot.find("a", recursive=False) and not derivedRoot.find("a", attrs={"class": "new"}, recursive=False):
                #     derivedLink = derivedRoot.find("a", recursive=False).get("href")
                #     parseWiktionaryPage(wiktionaryURL=derivedLink, rootLanguageCode=rootLanguageCode, parentLanguageCode=inputLanguageCode, inputLanguageCode=inputLanguageCode, parentWord=outputWord, rootWord=rootWord, inputWord=derivedWord, inputMeaning=inputMeaning, derivedTerm=derivedWord, isBorrowed=False)

                if term.find("ul"):
                    childList = term.find("ul").find_all("li", recursive=False)

                    for child in childList:
                        childElement = child.find("span", recursive=False)
                        if childElement:
                            child_a = child.find("a")
                            if child_a: 
                                childLang = childElement.get("lang")
                                childLink = child_a.get("href")
                                
                                if child_a: 
                                    childWord = child_a.contents[0]
                                    # work out if we need to parse this page
                                    # wordExists = collection.find({"lang": childLang, "rootLatinTranscription": rootWord}).count()
                                    # print(childElement.getText())
                                    # print(child_a.getText())
                                    if outputWord and childElement.getText() == child_a.getText():
                                        parseWiktionaryPage(wiktionaryURL=childLink, rootLanguageCode=rootLanguageCode, parentLanguageCode=inputLanguageCode, inputLanguageCode=childLang, parentWord=outputWord, rootWord=rootWord, inputWord=childWord, inputMeaning=inputMeaning, derivedTerm=derivedWord, isBorrowed=isBorrowed)
                                else: 
                                    childWord = childElement.contents[0]
                            

                                # parse descendants
                                childSublist = child.find("ul")
                                if childSublist:
                                    childDescendants = childSublist.find_all("li", recursive=False)
                                    parseDescendantList(childDescendants, childLang, childWord)

                        else: # the child is possibly just introducing another list
                            childSublist = child.find("ul")
                            if childSublist:
                                childDescendants = childSublist.find_all("li", recursive=False)
                                parseDescendantList(childDescendants, inputLanguageCode, outputWord)



        return


    def parseDescendantList(descendants, parentLanguageCode, parentWord):
        for descendant in descendants:

            if isinstance(descendant, Tag):         
                # descendant_a = descendant.find("a")
                isBorrowed = False
                if descendant.find("span", recursive=False):
                    derivedByAdditionOfMorphemes = descendant.find("span", attrs={"title": "derived by addition of morphemes"}, recursive=False)
                    if derivedByAdditionOfMorphemes is None:  
                        if descendant.contents[0]:
                            if isinstance(descendant.contents[0], Tag):
                                isBorrowed = True if descendant.find("span", attrs={"title": "borrowed"}, recursive=False) else False
                            else:      
                                # print(descendant.contents[0])
                                isBorrowed = True if descendant.find("span", attrs={"title": "borrowed"}, recursive=False) or descendant.contents[0][0] == "→" else False
                        elif descendant.contents:
                            isBorrowed = True if descendant.find("span", attrs={"title": "borrowed"}, recursive=False) or descendant.contents[0] == "→" else False
                        else:
                            isBorrowed = True if descendant.find("span", attrs={"title": "borrowed"}, recursive=False) else False
                    # do we have weirdness with the descendants actually being a list of derived terms?
                    if descendant.find("span", recursive=False):
                        if descendant.find("span", recursive=False).get("lang") == inputLanguageCode:
                            parseDerivedTerms(descendant, parentWord)
                        
                    # try to find a link
                    descendant_a = None
                    descendantSpans = descendant.find_all("span", recursive=False) 
                    # try and get a link which leads somewhere if possible
                    for span in descendantSpans:
                        if span.find("a"):
                            descendant_a = span.find("a")
                            if descendant_a.get("class"):
                                if descendant_a.get("class")[0] != "new":
                                    break
                            else: # if there's no class then the link isn't red
                                break
                    
                    if descendant_a and descendant_a != -1: 
                        # print("descendant_a")
                        # print(descendant_a)
                        descendantLink = descendant_a.get("href")

                        try:
                            # print(descendant_a.parent)
                            # print("descendant_a.parent")
                            if descendant_a.parent.get("class")==["Latn"] or descendant_a.parent.get("class")==["Latinx"]:
                                descendantWord = descendant_a.contents[0]
                            else:
                                descendantWord = descendant.find(attrs={"class": ["Latinx", "Latn", "None"]}).contents[0]
                            # print("DescendantWord: " + descendantWord)
                            if descendant.find(attrs={"class": ["Latn", "Latinx"]}):
                                # print('descendant.find(attrs={"class": ["Latn", "Latinx"]})')
                                # print(descendant.find(attrs={"class": ["Latn", "Latinx"]}))
                                descendantLang = descendant.find(attrs={"class": ["Latn", "Latinx"]}).get("lang")
                                
                                # if the descendantLang is the same as the inputLang, then we're dealing
                                # with a derived term, so ABORT because wiktionary has displayed things
                                # a bit incorrectly
                                if descendantLang == inputLanguageCode:
                                    parseDerivedTerms(descendant, parentWord)
                                else: # we're good, it's a real descendant                      
                                    # first parse the link
                                    parseWiktionaryPage(wiktionaryURL=descendantLink, rootLanguageCode=rootLanguageCode, parentLanguageCode=parentLanguageCode, inputLanguageCode=descendantLang, rootWord=rootWord, parentWord=parentWord, inputWord=descendantWord, inputMeaning=outputMeaning, derivedTerm=None, isBorrowed=isBorrowed)
                            
                                    # next parse any sublists
                                    if (descendant.find("ul", recursive=False)):
                                        parseDescendantList(descendant.find("ul", recursive=False), descendantLang, descendantWord)
                        except:
                            PrintException()
                            pass
                else: # the child is possibly just introducing another list
                    childSublist = descendant.find("ul")
                    if childSublist:
                        childDescendants = childSublist.find_all("li", recursive=False)
                        parseDescendantList(childDescendants, parentLanguageCode, parentWord)


    # first find all transliterated headwords in the right lang
    headword=None
    headwordList = searchResponse.find_all(attrs={"class": "headword-tr", "lang": inputLanguageCode})
   
    # iterate through list until we find one that matches our input word
    # (diacritics are dicks)
    for word in headwordList:
        outputWord = word
        while isinstance(outputWord, Tag): 
            outputWord = outputWord.contents[0]
            if headword is None:
                headword = word        
                if outputWord == inputWord:
                    headword = word
                    break


    if len(headwordList)==0: #, then the word probably naturally appears in latin script
        # find headword in latin script
        headwordList = searchResponse.find_all(attrs={"class": ["headword"], "lang": inputLanguageCode})

        # iterate through list until we find one that matches our input word
        # (diacritics are dicks)
        for word in headwordList:
            outputWord = word
            while isinstance(outputWord, Tag):         
                outputWord = outputWord.contents[0]
                if headword is None:
                    headword = word        
                    if outputWord == inputWord:
                        headword = word
                        break


    # should always be true, so probably don't need this
    if headword:
        outputWord = headword     
        while isinstance(outputWord, Tag):         
            # set the word we're inserting (its latin transcription)
            outputWord = outputWord.contents[0] 

        # let's try and find the meaning
        if headword.find_next("li"):
            if headword.find_next("li")!=headword.find_next("li", attrs={"class": "mw-empty-elt"}):
                meaning = headword.find_next("li").getText()

                # the meaning might contain multiple ways of explaining it.
                # we only want the first one
                commaIndex = meaning.find(",")
                semicolonIndex = meaning.find(";")
                if commaIndex != -1 and semicolonIndex != -1:
                    splitIndex = min(commaIndex, semicolonIndex)
                elif commaIndex == -1 and semicolonIndex != -1:
                    splitIndex = semicolonIndex
                elif commaIndex != -1 and semicolonIndex == -1:
                    splitIndex = commaIndex
                else:
                    splitIndex = -1
                # if there's a splitter, only take the string up to that point
                outputMeaning = meaning[0:splitIndex] if splitIndex != -1 else meaning
            
            else:
                meaning = headword.find_next("li").find_next("li").getText()
                commaIndex = meaning.find(",")
                semicolonIndex = meaning.find(";")
                if commaIndex != -1 and semicolonIndex != -1:
                    splitIndex = min(commaIndex, semicolonIndex)
                elif commaIndex == -1 and semicolonIndex != -1:
                    splitIndex = semicolonIndex
                elif commaIndex != -1 and semicolonIndex == -1:
                    splitIndex = commaIndex
                else:
                    splitIndex = -1
                outputMeaning = meaning[0:splitIndex] if splitIndex != -1 else meaning
        else: # if we can't find a new meaning, assume the meaning hasn't changed
            outputMeaning = inputMeaning

        # if we're on the root code, pieWord is the headword
        if inputLanguageCode == rootLanguageCode:
            pieWord = outputWord

        # find list of descendants 

        # find next headword of a different language
        next_headword = headword.find_next(attrs={"class": ["headword", "headword-tr"]})
        while next_headword:
            if next_headword.get("lang")==inputLanguageCode or (next_headword.get("lang") + "-Latn") == inputLanguageCode:
                next_headword = next_headword.find_next(attrs={"class": ["headword", "headword-tr"]})
            else:
                break

        # first if there's a next headword (i.e. other languages on that page)
        if next_headword:
            # only process descendants if they belong to the correct language
            if headword.find_next(attrs={"id": ["Descendants", "Descendants_2", "Descendants_3", "Descendants_4", "Descendants_5", "Descendants_6"]}) != next_headword.find_next(attrs={"id": ["Descendants", "Descendants_2", "Descendants_3", "Descendants_4", "Descendants_5", "Descendants_6"]}):
                descendantHeader = headword.find_next(attrs={"id": ["Descendants", "Descendants_2", "Descendants_3", "Descendants_4", "Descendants_5", "Descendants_6"]})
                descendantList = descendantHeader.find_next("ul")
                descendantItems = descendantList.find_all("li", recursive=False)
                parseDescendantList(descendantItems, inputLanguageCode, outputWord)
                try:
                    while descendantList.next_sibling.next_sibling.name=="ul":
                        # print("got here (1)")
                        descendantList = descendantList.next_sibling.next_sibling
                        descendantItems = descendantList.find_all("li", recursive=False)
                        parseDescendantList(descendantItems, inputLanguageCode, outputWord)
                except: # no more siblings
                    pass
            # only process derived terms if they belong to the correct language
            if headword.find_next(attrs={"id":"Derived_terms"}) != headword.find_next(attrs={"class": ["headword", "headword-tr"]}).find_next(attrs={"id":"Derived_terms"}) and inputLanguageCode==rootLanguageCode:
                derivedTermsHeader = headword.find_next(attrs={"id":"Derived_terms"})
                # print("------------------------------------------------------")
                # print("DERIVED TERMS HEADER")
                # print("------------------------------------------------------")
                if derivedTermsHeader:
                    derivedTermsList = derivedTermsHeader.find_next("ul").find_all("li", recursive=False)
                    parseDerivedTerms(derivedTermsList, outputWord)

       # if there are no more languages on that page, things are easier
        elif next_headword is None:
            descendantHeader = headword.find_next(attrs={"id": ["Descendants", "Descendants_2", "Descendants_3", "Descendants_4", "Descendants_5", "Descendants_6"]})
            if descendantHeader:
                descendantList = descendantHeader.find_next("ul")
                descendantItems = descendantList.find_all("li", recursive=False)
                parseDescendantList(descendantItems, inputLanguageCode, outputWord)
                try:
                    while descendantList.next_sibling.next_sibling.name=="ul":
                        # print("got here (2)")
                        descendantList = descendantList.next_sibling.next_sibling
                        descendantItems = descendantList.find_all("li", recursive=False)
                        parseDescendantList(descendantItems, inputLanguageCode, outputWord)
                except: # no more siblings
                    pass
            derivedTermsHeader = headword.find_next(attrs={"id":"Derived_terms"})
                # print("------------------------------------------------------")
                # print("DERIVED TERMS HEADER")
                # print("------------------------------------------------------")
                # print(derivedTermsHeader)
            if (derivedTermsHeader and inputLanguageCode==rootLanguageCode):
                derivedTermsList = derivedTermsHeader.find_next("ul").find_all("li", recursive=False)
                parseDerivedTerms(derivedTermsList, outputWord)

        # find IPA Transcription of headword
        try:
            if headword.find_previous(attrs={"class": ["headword", "headword-tr"]}):
                if headword.find_previous(attrs={"class": "IPA"}) != headword.find_previous(attrs={"class": ["headword", "headword-tr"]}).find_previous(attrs={"class": "IPA"}):
                    ipaTranscription = headword.find_previous(attrs={"class": "IPA"}).find_parent("ul").find(attrs={"class": "IPA"}).contents[0]
                    #print(ipaTranscription)
            else:
                ipaTranscription = None
        except Exception as e:
            print(e)
            ipaTranscription = None
            pass


    else: # otherwise language does not exist on that page (we were duped!)
        # but luckily we have enough information from the input vars
        outputWord = inputWord
        ipaTranscription = None
        outputMeaning = inputMeaning


    try:
        collection.insert({"lang": inputLanguageCode, "parentLang": parentLanguageCode, "rootLang": rootLanguageCode, "latinTranscription": outputWord, "parentLatinTranscription":  parentWord,  "rootLatinTranscription": rootWord, "ipaTranscription": ipaTranscription, "meaning": outputMeaning, "url": wiktionaryURL, "derivedRoot": derivedTerm, "isBorrowed": isBorrowed, "tags": [], "soundChanges": []})
        print("inserted " + outputWord + " for " + inputLanguageCode)
    except pymongo.errors.DuplicateKeyError:
        print("duplicate key error with "  + outputWord + " for " + inputLanguageCode)
        pass
    except Exception as e:
        print(e)
        pass
        
    searchResponse.decompose()
    return

# lemmaURL = "https://en.m.wiktionary.org/w/index.php?title=Category:Proto-Indo-European_lemmas&pagefrom=H3ER-%0AProto-Indo-European%2Fh%E2%82%83er-#mw-pages" 
# lemmaURL = "https://en.m.wiktionary.org/w/index.php?title=Category:Proto-Indo-European_lemmas&pagefrom=MEH2D-%0AProto-Indo-European%2Fmeh%E2%82%82d-#mw-pages" 
# lemmaURL = "https://en.m.wiktionary.org/w/index.php?title=Category:Proto-Indo-European_lemmas&pagefrom=SEM%0AProto-Indo-European%2Fs%E1%B8%97m#mw-pages"
#lemmaURL = "https://en.wiktionary.org/wiki/Reconstruction:Proto-Indo-Iranian/d%CA%B0%C3%A1%C7%B0%CA%B0ati"
# iteration=0
# parseWiktionaryPage("/wiki/Reconstruction:Proto-Indo-European/bʰer-", "NONE", "ine-pro", "NONE", None, None, None, None)
parseWiktionaryPage(wiktionaryURL="/wiki/Reconstruction:Proto-Indo-European/suHn%C3%BAs", rootLanguageCode="ine-pro", parentLanguageCode="NONE", inputLanguageCode="ine-pro", rootWord="*suh₁nús", parentWord=None, inputWord="*suh₁nús", inputMeaning=None, derivedTerm=None, isBorrowed=False)
# parseWiktionaryPage(wiktionaryURL="https://en.wiktionary.org/wiki/Reconstruction:Proto-Indo-European/b%CA%B0leh%E2%82%83-", rootLanguageCode="ine-pro", parentLanguageCode="ine-pro", inputLanguageCode="gem-pro", rootWord="*h₁néwn̥", parentWord="*h₁néwn̥", inputWord="*newun", inputMeaning="nine", derivedTerm=None, isBorrowed=False)

print("inserted test word")
time.sleep(2000)


pieLangCode = "ine-pro"
uralicLangCode = "urj-pro"
semiticLangCode = "sem-pro"
austronesianLangCode = "map-pro"
trkLangCode = "trk-pro"

# lemmaURL = "https://en.wiktionary.org/wiki/Category:Proto-Uralic_lemmas"
# lemmaURL = "https://en.wiktionary.org/wiki/Category:Proto-Austronesian_lemmas"
# lemmaURL = "https://en.wiktionary.org/wiki/Category:Proto-Semitic_lemmas"
# lemmaURL = "https://en.wiktionary.org/wiki/Category:Proto-Turkic_lemmas"
# lemmaURL = "https://en.wiktionary.org/w/index.php?title=Category:Proto-Indo-European_lemmas&pagefrom=G%27ENUS%0AProto-Indo-European%2F%C7%B5%C3%A9nus#mw-pages"
# lemmaURL = "https://en.wiktionary.org/w/index.php?title=Category:Proto-Indo-European_lemmas&pagefrom=H2ELYOS%0AProto-Indo-European%2Fh%E2%82%82%C3%A9lyos#mw-pages"
# lemmaURL = "https://en.wiktionary.org/w/index.php?title=Category:Proto-Indo-European_lemmas&pagefrom=KEH3-%0AProto-Indo-European%2Fkeh%E2%82%83-#mw-pages"

#tempurl
# lemmaURL = "https://en.wiktionary.org/w/index.php?title=Category:Proto-Indo-European_lemmas&pagefrom=p#mw-pages"


# print(lemma_searchResponse)
# print(lemma_searchResponse.find("div", attrs={"class": "mw-content-ltr"}))

# iteration = 0
def parseLemmas(url):
    global iteration
    lemma_request_object = requests.get(url)
    lemma_searchResponse = BeautifulSoup(lemma_request_object.content, 'html.parser')
    lemmas = lemma_searchResponse.find(attrs={"id": "mw-pages"}).find_all("li")
    for lemma in lemmas:
        print(lemma.find("a").contents[0])
        rootWord = "*" + re.split("/", lemma.find("a").contents[0], 1)[1]
        lemmaHref = lemma.find("a").get("href")
        iteration=0
        parseWiktionaryPage(wiktionaryURL=lemmaHref, rootLanguageCode=rootLang, parentLanguageCode="NONE", inputLanguageCode=rootLang, rootWord=rootWord, parentWord=None, inputWord=rootWord, inputMeaning=None, derivedTerm=None, isBorrowed=False)

rootLang = pieLangCode
lemmaURL = "https://en.m.wiktionary.org/w/index.php?title=Category:Proto-Indo-European_lemmas" 
parseLemmas(lemmaURL)
# lemmaURL = "https://en.m.wiktionary.org/w/index.php?title=Category:Proto-Indo-European_lemmas&pagefrom=G%27ENH1TRIH2%0AProto-Indo-European%2F%C7%B5%C3%A9nh%E2%82%81trih%E2%82%82#mw-pages" 
# parseLemmas(lemmaURL)
# lemmaURL = "https://en.m.wiktionary.org/w/index.php?title=Category:Proto-Indo-European_lemmas&pagefrom=H2ELTEROS%0AProto-Indo-European%2Fh%E2%82%82%C3%A9lteros#mw-pages" 
# parseLemmas(lemmaURL)
# lemmaURL = "https://en.m.wiktionary.org/w/index.php?title=Category:Proto-Indo-European_lemmas&pagefrom=KEH2N-%0AProto-Indo-European%2Fkeh%E2%82%82n-#mw-pages" 
# parseLemmas(lemmaURL)
# lemmaURL = "https://en.m.wiktionary.org/w/index.php?title=Category:Proto-Indo-European_lemmas&pagefrom=PELH2-%0AProto-Indo-European%2Fpelh%E2%82%82-#mw-pages" 
# parseLemmas(lemmaURL)
# lemmaURL = "https://en.m.wiktionary.org/w/index.php?title=Category:Proto-Indo-European_lemmas&pagefrom=STEH2YETI%0AProto-Indo-European%2Fst%C3%A9h%E2%82%82yeti#mw-pages" 
# parseLemmas(lemmaURL)
# lemmaURL = "https://en.m.wiktionary.org/w/index.php?title=Category:Proto-Indo-European_lemmas&pagefrom=WOK%27EH2%0AProto-Indo-European%2Fwo%E1%B8%B1%C3%A9h%E2%82%82#mw-pages"
# parseLemmas(lemmaURL)


lemmaURL = "https://en.m.wiktionary.org/w/index.php?title=Category:Proto-Indo-European_lemmas&pagefrom=W#mw-pages"
parseLemmas(lemmaURL)


# rootLang = semiticLangCode

# lemmaURL = "https://en.m.wiktionary.org/wiki/Category:Proto-Semitic_lemmas"
# parseLemmas(lemmaURL)

# rootLang = uralicLangCode

# lemmaURL = "https://en.m.wiktionary.org/wiki/Category:Proto-Uralic_lemmas"
# parseLemmas(lemmaURL)
# lemmaURL = "https://en.m.wiktionary.org/w/index.php?title=Category:Proto-Uralic_lemmas&pagefrom=T%C3%84%0AProto-Uralic%2Ft%C3%A4#mw-pages"
# parseLemmas(lemmaURL)

# rootLang="dra-pro"
# lemmaURL="https://en.m.wiktionary.org/wiki/Category:Proto-Dravidian_lemmas"
# parseLemmas(lemmaURL)

# rootLang="bnt-pro"
# lemmaURL ="https://en.m.wiktionary.org/wiki/Category:Proto-Bantu_lemmas"
# parseLemmas(lemmaURL)

# rootLang = austronesianLangCode
# lemmaURL = "https://en.m.wiktionary.org/wiki/Category:Proto-Austronesian_lemmas"
# parseLemmas(lemmaURL)

# rootLang="jpx-pro"
# lemmaURL="https://en.m.wiktionary.org/wiki/Category:Proto-Japonic_lemmas"
# parseLemmas(lemmaURL)

# rootLang=trkLangCode
# lemmaURL="https://en.m.wiktionary.org/wiki/Category:Proto-Turkic_lemmas"
# parseLemmas(lemmaURL)

# rootLang="mkh-pro"
# lemmaURL="https://en.m.wiktionary.org/wiki/Category:Proto-Mon-Khmer_lemmas"
# parseLemmas(lemmaURL)

# rootLang="xgn-pro"
# lemmaURL="https://en.m.wiktionary.org/wiki/Category:Proto-Mongolic_lemmas"
# parseLemmas(lemmaURL)



print("DONE WITH INSERT, CHECK IT")



