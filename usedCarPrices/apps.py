from django.apps import AppConfig


class UsedcarpricesConfig(AppConfig):
    name = 'usedCarPrices'
