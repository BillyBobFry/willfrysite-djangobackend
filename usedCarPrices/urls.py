from django.urls import path

from . import views

urlpatterns = [
    path('get-categories', views.getCategories, name='getCategories'),
    path('get-cars-in-budget', views.getCarsInBudget, name='getCarsInBudget'),
    path('', views.index, name='index'),
]