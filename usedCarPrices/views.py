from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import re
import json
import time
from datetime import datetime
from bson.son import SON
import pymongo
import array
from pymongo import MongoClient

client = MongoClient('localhost', 27017)
db = client.usedCarPrices
collectionName = f'prices{datetime.today().year}{datetime.today().month}'
collection = db[collectionName]


# Create your views here.
def index(request):
    requestBody = json.loads(request.body)
    requestType = requestBody['requestType']

    if requestType == "count":
        return JsonResponse({"count": collection.count()})

    if requestType == "getMakes":
        #makes = collection.find().distinct("make")
        makeCursor = collection.aggregate([{
            "$group": {
                "_id": "$make",
                "count": {
                    "$sum": 1
                }
            }
        }, {
            "$sort": SON([("_id", 1)])
        }])
        makes = []
        for make in makeCursor:
            makes.append(make)

        return JsonResponse({"makes": makes})

    if requestType == "getModels":
        make = requestBody['make']
        #models = collection.find({"make": make}).aggregate([{"$group": {"_id": "$model", "count": {"$sum":1}}}])
        modelCursor = list(
            collection.aggregate([{
                "$match": {
                    "make": make
                }
            }, {
                "$group": {
                    "_id": "$model",
                    "count": {
                        "$sum": 1
                    }
                }
            }, {
                "$sort": SON([("_id", 1)])
            }]))

        models = []
        for model in modelCursor:
            models.append(model)

        return JsonResponse({"models": models})

    if requestType == "getResults":
        optionList = requestBody['options']
        for option in optionList:
            print(option)
        resultsByPrice = list(
            collection.find({
                "model": {
                    "$in": optionList
                }
            }, {
                "_id": 0,
                "make": 1,
                "model": 1,
                "mileage": 1,
                "vehicle_price": 1,
                "vehicle_year": 1,
            }).sort([("vehicle_price", pymongo.DESCENDING)]))
        resultsByMileage = list(
            collection.find({
                "model": {
                    "$in": optionList
                }
            }, {
                "_id": 0,
                "make": 1,
                "model": 1,
                "mileage": 1,
                "vehicle_price": 1
            }).sort([("mileage", pymongo.DESCENDING)]))
        returnVar = []
        max_mileage = []
        for result in resultsByPrice:
            returnVar.append(json.dumps(result))
        for result in resultsByMileage:
            max_mileage.append(json.dumps(result))
        response = JsonResponse({
            "results": returnVar,
            "max_mileage": max_mileage
        })
        return response


def getCategories(request):
    print(request)

    bodyTypes = collection.distinct("body_type")
    gears = collection.distinct("gears")
    doors = collection.distinct("doors")

    print(bodyTypes)
    return JsonResponse({
        "bodyTypes": bodyTypes,
        "gears": gears,
        "doors": doors
    })


def getCarsInBudget(request):
    req = json.loads(request.body)
    bodyTypes = req["bodyTypes"]
    doors = req["doors"]
    gears = req["gears"]
    minMileage = int(req["minMileage"])
    maxMileage = int(req["maxMileage"])
    maxPrice = int(req["maxPrice"])
    minYear = int(req["minYear"])
    maxYear = int(req["maxYear"])

    # get all listings that match input conditions (except budget)
    results = collection.aggregate([{
        "$match": {
            "doors": {
                "$in": doors
            },
            "gears": {
                "$in": gears
            },
            "body_type": {
                "$in": bodyTypes
            },
            "mileage": {
                "$gt": minMileage,
                "$lt": maxMileage
            },
            "vehicle_year": {
                "$gte": minYear,
                "$lte": maxYear
            }
        }
    }, {
        "$project": {
            "_id": 0,
            "makeModel": {
                "$concat": ["$make", " ", "$model"]
            },
            "underBudget": {
                "$cond": [{
                    "$lt": ["$vehicle_price", maxPrice]
                }, 1, 0]
            },
            "overBudget": {
                "$cond": [{
                    "$gt": ["$vehicle_price", maxPrice]
                }, 1, 0]
            }
        }
    }, {
        "$group": {
            "_id": "$makeModel",
            "countUnder": {
                "$sum": "$underBudget"
            },
            "countOver": {
                "$sum": "$overBudget"
            }
        }
    }])
    results = list(results)
    print(results)
    # for result in results:
    #     print(f'{mileage}, {result["underMileage"]}, {result["overMileage"]}, {result["mileage"]}, {result["mileage"] < mileage}')

    return JsonResponse({"success": results})
