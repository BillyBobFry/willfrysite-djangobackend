"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path, path

urlpatterns = [
    path('admin/', admin.site.urls),
    path('django/trafficData/', include('trafficData.urls')),
    path('django/fantasyPLAPI/', include('fantasyPLAPI.urls')),
    path('django/usedCarPrices/', include('usedCarPrices.urls')),
    path('django/londonTrainDelay/', include('londonTrainDelay.urls')),
    path('django/baby-names/', include('baby-names.urls')),
    path('django/indo-european-cognates/',
         include('indo-european-cognates.urls')),
    path('django/indo-european-cognates-2/',
         include('indo-european-cognates-2.urls')),
    path('django/doublets/', include('doublets.urls')),
    path('django/linguistics-descendant-finder/',
         include('linguistics-descendant-finder.urls')),
    path('django/uk-climate/', include('uk-climate.urls')),
    path('django/trek-scripts/', include('trek-scripts.urls')),
    path('django/etymology-map/', include('etymology-map.urls')),
    path('django/script-parser/', include('script-parser.urls')),
    path('media/', include('trafficData.urls')),
]
