from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import json
import re
import time, datetime
import pymongo
import array
from pymongo import MongoClient
import xml.etree.ElementTree as ET
import requests

client = MongoClient('localhost', 27017)
db = client.babynames
collection = db["babynames"]

headers = {'content-type': 'text/xml'}


# Simply return all stations that have had their delays queried
def index(request):
    requestBody = json.loads(request.body)
    requestType = requestBody['requestType']
    if requestType == "getDistinctNames":
        distinctNames = collection.aggregate([{
            "$group": {
                "_id": {
                    "name": "$name",
                    "gender": "$gender"
                }
            }
        }, {
            "$sort": {
                "name": 1
            }
        }])

        names = []

        for name in distinctNames:
            if isinstance(
                    name["_id"]["name"],
                    str):  # no clue why, but one of these names is corrupted
                names.append({
                    "name": name["_id"]["name"],
                    "gender": name["_id"]["gender"]
                })

        return JsonResponse({"names": names})

    elif requestType == "getNamesFromSubstring":
        names = []
        substring = requestBody["substring"]
        regx = re.compile('^' + substring, re.IGNORECASE)
        nameCursor = collection.aggregate([{
            "$match": {
                "name": {
                    "$regex": regx
                }
            }
        }, {
            "$group": {
                "_id": {
                    "name": "$name",
                    "gender": "$gender"
                }
            }
        }, {
            "$sort": {
                "_id.name": 1
            }
        }])

        for name in nameCursor:
            names.append({
                "displayName": name["_id"]["name"],
                "value": name["_id"]["name"],
                "extra": name["_id"]["gender"]
            })

        return JsonResponse({"names": names})

    else:
        boyNameList = requestBody["boyNames"]
        girlNameList = requestBody["girlNames"]

        boyQueryList = []
        girlQueryList = []

        for name in boyNameList:
            boyQueryList.append(name.upper())
        for name in girlNameList:
            girlQueryList.append(name.upper())

        boyFind = collection.find(
            {
                "name": {
                    "$in": boyQueryList
                },
                "gender": "boy"
            }, {
                "name": 1,
                "year": 1,
                "gender": 1,
                "rank": 1,
                "count": 1,
                "_id": 0
            }).sort([("year", pymongo.ASCENDING)])
        girlFind = collection.find(
            {
                "name": {
                    "$in": girlQueryList
                },
                "gender": "girl"
            }, {
                "name": 1,
                "year": 1,
                "gender": 1,
                "rank": 1,
                "count": 1,
                "_id": 0
            }).sort([("year", pymongo.ASCENDING)])

        boyNames = []
        girlNames = []

        for boyRecord in boyFind:
            boyNames.append(json.dumps(boyRecord))
        for girlRecord in girlFind:
            girlNames.append(json.dumps(girlRecord))

        return JsonResponse({"boys": boyNames, "girls": girlNames})
