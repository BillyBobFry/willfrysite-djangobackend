import requests
from bs4 import BeautifulSoup, Tag, NavigableString
import re
import json
import time
import datetime
from pymongo import MongoClient
import array
import random
import uuid
import sys
from neo4j import GraphDatabase
import linecache

sys.setrecursionlimit(10000)


# set neo4j vars
uri = "bolt://localhost:7687"
driver = GraphDatabase.driver(uri, auth=("neo4j", "pass"), encrypted=False)
outfile = open("output.txt", "a+", encoding="utf-8")
outfile.write("Test")
# set urls

testMode = 0
testURL = '/wiki/Reconstruction:Proto-Indo-European/gʷṓws'
testURL = '/wiki/Reconstruction:Proto-Indo-European/bʰer-'
# testURL = '/wiki/Reconstruction:Proto-Indo-European/dʰewh₂-'
testWord = '*bʰer-'
testLang = "ine-pro"


def insertGraphWord(tx, word):
    if word["languageName"] != "NONE":
        existingWord = findWord(tx, word)
        # print("have a peek:")
        # print(existingWord.peek())
        wordExists = False if existingWord.peek() is None else True
        # print(
        #     f"word {word['latinTranscription']} exists in {word['languageName']}? {wordExists}")
        if wordExists == False:
            newWord = tx.run(
                f'CREATE (a:word {dict2Cypher(word)}) return a')
            return newWord
        else:
            return(existingWord)


def insertLink(tx, a, b, linkType):

    insertGraphWord(tx, a)
    # time.sleep(0.5)
    insertGraphWord(tx, b)
    # time.sleep(0.5)

    existingLink = tx.run(f'match (a:word {dict2Cypher(a)})-[r:{linkType}]->(b:word {dict2Cypher(b)})'
                          f" return r")
    linkExists = False if existingLink.peek() is None else True
    # print(
    #     f"{a['languageName']} {a['latinTranscription']} -> {b['languageName']} {b['latinTranscription']}")
    # print(f"link already exists? {linkExists}")
    if not linkExists:

        newLink = tx.run(f'match (a:word),(b:word)'
                         f' where a.languageCode = "{a["languageCode"]}"'
                         f' and a.languageName = "{a["languageName"]}"'
                         f' and a.latinTranscription = "{a["latinTranscription"]}"'
                         f' and b.languageCode = "{b["languageCode"]}"'
                         f' and b.languageName = "{b["languageName"]}"'
                         f' and b.latinTranscription = "{b["latinTranscription"]}"'
                         f" create (a)-[r:{linkType}]->(b)"
                         f" return r")
        # print(f"link exists now? {newLink.peek()!=None}")
        return newLink

    return existingLink


def findWord(tx, word):

    results = tx.run(f'match (a:word) '
                     f' where a.languageCode = "{word["languageCode"]}"'
                     f' and a.languageName = "{word["languageName"]}"'
                     f' and a.latinTranscription = "{word["latinTranscription"]}"'
                     f' return a')

    return results


def dict2Cypher(inputDict):
    return re.sub(r'"(\w+)":', r'\1:', json.dumps(inputDict))

# with driver.session() as session:
#     wordA={'languageCode': 'en', 'languageName': 'English',
#              'latinTranscription': 'testenglish'}
#     wordB = {'languageCode': 'srn', 'languageName': 'Sranan Tongo',
#               'latinTranscription': 'testsranan'}
#     resplink = session.read_transaction(
#         insertLink, wordA, wordB, "PARENT")
#     print(resplink)
# print("Done. Having a nap.")
# time.sleep(20)


recentPages = []
for i in range(1000):
    recentPages.append(i)


def PrintException():
    exc_type, exc_obj, tb = sys.exc_info()
    f = tb.tb_frame
    lineno = tb.tb_lineno
    filename = f.f_code.co_filename
    linecache.checkcache(filename)
    line = linecache.getline(filename, lineno, f.f_globals)
    print('EXCEPTION IN ({}, LINE {} "{}"): {}'.format(filename, lineno,
                                                       line.strip(), exc_obj))


client = MongoClient('localhost', 27017)
db = client.linguistics
collection = db["doublets"]

headers = {'content-type': 'text/xml'}

distinctLanguages = list(map(
    lambda e: {"code": e["code"], "name": e["name"]}, list(db["languages"].find())))
# print(distinctLanguages)
# time.sleep(10)


def getCodeFromName(languageName):
    allNames = list(map(lambda e: e["name"], distinctLanguages))
    allCodes = list(map(lambda e: e["code"], distinctLanguages))
    i = allNames.index(languageName)

    return allCodes[i]


def getNameFromCode(languageCode):
    allNames = list(map(lambda e: e["name"], distinctLanguages))
    allCodes = list(map(lambda e: e["code"], distinctLanguages))
    try:
        i = allCodes.index(languageCode.replace("-Latn", ""))
    except:
        return languageCode
    return allNames[i]


rootURL = 'https://en.wiktionary.org'
pieNounsURL = 'https://en.m.wiktionary.org/wiki/Appendix:List_of_Proto-Indo-European_nouns'
iteration = 0


def parseWiktionaryPage(wiktionaryURL, rootLanguageCode, parentLanguageCode,
                        inputLanguageCode, rootWord, parentWord, inputWord,
                        inputMeaning, derivedTerm, isBorrowed, parentID,
                        rootID):

    # be nice to wiktionary and have a little snooze.
    # but only if we're not testing
    if testMode == 0:
        time.sleep(0.5)

    # don't go round in loops
    print(f"parsing {wiktionaryURL}")
    if recentPages.count(wiktionaryURL) > 0:
        # print(f"GOING ROUND IN A LOOP, MAN. ALREADY PARSED {wiktionaryURL}")
        # print(recentPages)
        return
    else:
        recentPages.pop(0)
        recentPages.append(wiktionaryURL)

    global iteration
    print(iteration)
    iteration = iteration + 1

    def insertWord(inputObject):
        try:
            if isinstance(inputObject["latinTranscription"], Tag):
                inputObject["latinTranscription"] = inputObject[
                    "latinTranscription"].getText()
            if isinstance(inputObject["parentLatinTranscription"], Tag):
                inputObject["parentLatinTranscription"] = inputObject[
                    "parentLatinTranscription"].getText()
            while isinstance(inputObject["ipaTranscription"], Tag):
                inputObject["ipaTranscription"] = inputObject[
                    "ipaTranscription"].contents[0]
            while isinstance(inputObject["meaning"], Tag):
                inputObject["meaning"] = inputObject["meaning"].contents[0]
            linkType = inputObject["linkType"] if "linkType" in inputObject.keys(
            ) else "PARENT"
            # print(inputObject)

            # make some things lower case
            inputObject["latinTranscription"] = inputObject["latinTranscription"].lower(
            ) if inputObject["latinTranscription"] else inputObject["latinTranscription"]
            inputObject["parentLatinTranscription"] = inputObject["parentLatinTranscription"].lower(
            ) if inputObject["parentLatinTranscription"] else inputObject["parentLatinTranscription"]

            with driver.session() as session:
                parentWord = {"languageCode": inputObject["parentLang"], "languageName": getNameFromCode(
                    inputObject["parentLang"]), "latinTranscription": inputObject["parentLatinTranscription"]}
                childWord = {"languageCode": inputObject["lang"], "languageName": getNameFromCode(
                    inputObject["lang"]), "latinTranscription": inputObject["latinTranscription"]}
                if inputObject["ipaTranscription"]:
                    childWord["ipaTranscription"] = inputObject["ipaTranscription"]
                newID = session.read_transaction(
                    insertLink, parentWord, childWord, linkType)

            print("inserted " + outputWord + " for " + inputLanguageCode)
            return newID
        except Exception as e:
            print("error in insertword()")
            print(e)
            return None
            pass

    if iteration > 1000:
        print("REACHED MAXIMUM ITERATIONS. SAY GOODBYE.")
        return
    ipaTranscription = None
    print(wiktionaryURL)

    # sometimes the urls take us to other places
    if wiktionaryURL[0:6] != "/wiki/" and wiktionaryURL[0:3] != "/w/":
        return

    outputMeaning = inputMeaning
    request_object = requests.get(rootURL + wiktionaryURL)
    searchResponse = BeautifulSoup(request_object.content, 'html.parser')

    def parseDerivedTerms(derivedTerms, parentWord):
        iter = 0

        for term in derivedTerms:
            termtext = term.getText() if isinstance(term, Tag) else ""
            if term.name == "ul":
                parseDescendantList(term.find_all("li", recursive=False), inputLanguageCode,
                                    parentWord, parentID)
            else:
                derivedID = parentID
                outputWord = inputWord
                # iter = iter+1
                if isinstance(term, Tag):
                    derivedRoot = term.find(attrs={"lang": inputLanguageCode})
                    if not derivedRoot:
                        if term.contents:
                            if term.get("lang") == inputLanguageCode:
                                derivedWord = term.contents[0]
                            elif str(term.contents[0].encode("utf-8"))[:22] == "b'Unsorted formations:":
                                derivedWord = inputWord
                            else:
                                derivedWord = inputWord

                            if isinstance(derivedWord, Tag):
                                derivedWord = derivedWord.getText()

                            # shall we try and parse the link?
                            derived_link = term.find("a")
                            if derived_link:
                                if derived_link.get("class"):
                                    if derived_link.get("class")[0] != "new":
                                        if derived_link.getText(
                                        ) == term.getText():
                                            derived_link_url = derived_link.get(
                                                "href")
                                            derivedID = parseWiktionaryPage(
                                                wiktionaryURL=derived_link_url,
                                                rootLanguageCode=rootLanguageCode,
                                                parentLanguageCode=inputLanguageCode,
                                                inputLanguageCode=inputLanguageCode,
                                                parentWord=outputWord,
                                                rootWord=rootWord,
                                                inputWord=derivedWord,
                                                inputMeaning=inputMeaning,
                                                derivedTerm=None,
                                                isBorrowed=False,
                                                parentID=parentID,
                                                rootID=rootID)
                                    # else: # else link is red, let's parse the sublist
                                else:  # if there's no class then the link isn't red
                                    if derived_link.getText() == term.getText(
                                    ):
                                        derived_link_url = derived_link.get(
                                            "href")
                                        derivedID = parseWiktionaryPage(
                                            wiktionaryURL=derived_link_url,
                                            rootLanguageCode=rootLanguageCode,
                                            parentLanguageCode=inputLanguageCode,
                                            inputLanguageCode=inputLanguageCode,
                                            parentWord=inputWord,
                                            rootWord=rootWord,
                                            inputWord=derivedWord,
                                            inputMeaning=inputMeaning,
                                            derivedTerm=None,
                                            isBorrowed=False,
                                            parentID=parentID,
                                            rootID=rootID)

                            if derivedWord != rootWord:  # sometimes the derived word is the word itself. This causes duplicate keys, and is dumb - why are they not just descendants?
                                try:
                                    derivedID = insertWord({
                                        "lang":
                                        inputLanguageCode,
                                        "parentLang":
                                        inputLanguageCode,
                                        "rootLang":
                                        rootLanguageCode,
                                        "latinTranscription":
                                        derivedWord,
                                        "parentLatinTranscription":
                                        parentWord,
                                        "rootLatinTranscription":
                                        rootWord,
                                        "ipaTranscription":
                                        None,
                                        "meaning":
                                        inputMeaning,
                                        "url":
                                        wiktionaryURL,
                                        "derivedRoot":
                                        None,
                                        "isBorrowed":
                                        False,
                                        "tags": [],
                                        "soundChanges": [],
                                        "parentID":
                                        parentID,
                                        "rootID":
                                        rootID,
                                        "linkType": "DERIVED"
                                    })
                                except Exception as e:
                                    print("error:")
                                    print(e)
                                    pass

                                outputWord = derivedWord
                                # print("inserted " + outputWord + " for " + inputLanguageCode)
                            else:
                                outputWord = derivedWord

                            derivedRoot = term
                        else:
                            continue

                    else:
                        derivedWord = derivedRoot.getText()
                        # print(f"DERIVED WORD IS {derivedWord}")
                        # while isinstance(derivedWord, Tag):
                        #     derivedWord = derivedWord.getText()

                        # shall we try and parse the link?
                        derived_link = derivedRoot.find("a", recursive=False)
                        if derived_link:
                            # if descendant_a.get("class")[0] != "new":
                            linkExists = True
                            if derived_link.get("class") and derived_link.get("class")[0] == "new":
                                linkExists = False
                            if derived_link.getText() == derivedRoot.getText() and linkExists:
                                derived_link_url = derived_link.get("href")
                                derivedID = parseWiktionaryPage(
                                    wiktionaryURL=derived_link_url,
                                    rootLanguageCode=rootLanguageCode,
                                    parentLanguageCode=inputLanguageCode,
                                    inputLanguageCode=inputLanguageCode,
                                    parentWord=inputWord,
                                    rootWord=rootWord,
                                    inputWord=derivedWord,
                                    inputMeaning=inputMeaning,
                                    derivedTerm=None,
                                    isBorrowed=False,
                                    parentID=parentID,
                                    rootID=rootID)
                            else:  # link is red
                                derivedID = insertWord({
                                    "lang":
                                    inputLanguageCode,
                                    "parentLang":
                                    inputLanguageCode,
                                    "latinTranscription":
                                    derivedWord,
                                    "parentLatinTranscription":
                                    parentWord,
                                    "rootLatinTranscription":
                                    rootWord,
                                    "ipaTranscription":
                                    None,
                                    "meaning":
                                    outputMeaning,
                                    "url":
                                    None,
                                    "derivedRoot":
                                    derivedWord,
                                    "linkType": "DERIVED"
                                })

                        else:  # it's not a link, but could have descendants
                            # first lets insert the derived term
                            derivedID = insertWord({
                                "lang":
                                inputLanguageCode,
                                "parentLang":
                                inputLanguageCode,
                                "latinTranscription":
                                derivedWord,
                                "parentLatinTranscription":
                                parentWord,
                                "ipaTranscription":
                                None,
                                "meaning":
                                inputMeaning,
                                "parentID":
                                parentID,
                                "linkType": "DERIVED"
                            })
                        # print(f"DERIVED ID: {derivedID}")
                        if derivedID:  # if the insert was successful
                            descendantList = term.find("ul")
                            # print(term)
                            # print(f"DESCENDANT LIST: {descendantList}")
                            if descendantList:
                                # print("FOUND LIST OF DESCENDANTS, PARSING")
                                descendants = descendantList.find_all(
                                    "li", recursive=False)
                                parseDescendantList(
                                    descendants, inputLanguageCode,
                                    derivedWord, derivedID)

                        outputWord = derivedWord

                        if derivedWord != rootWord:  # sometimes the derived word is the word itself. This causes duplicate keys, and is dumb - why are they not just descendants?
                            try:
                                derivedID = insertWord({
                                    "lang":
                                    inputLanguageCode,
                                    "parentLang":
                                    inputLanguageCode,
                                    "rootLang":
                                    rootLanguageCode,
                                    "latinTranscription":
                                    derivedWord,
                                    "parentLatinTranscription":
                                    parentWord,
                                    "rootLatinTranscription":
                                    rootWord,
                                    "ipaTranscription":
                                    None,
                                    "meaning":
                                    inputMeaning,
                                    "url":
                                    wiktionaryURL,
                                    "derivedRoot":
                                    None,
                                    "isBorrowed":
                                    False,
                                    "tags": [],
                                    "soundChanges": [],
                                    "parentID":
                                    parentID,
                                    "rootID":
                                    rootID,
                                    "linkType": "DERIVED"
                                })
                            except Exception as e:
                                print("error:")
                                print(e)
                                pass

                            outputWord = derivedWord
                            # print("inserted " + outputWord + " for " + inputLanguageCode)
                            # print(e)

                    if term.find("ul"):
                        childList = term.find("ul").find_all("li",
                                                             recursive=False)

                        for child in childList:
                            childID = derivedID
                            childElement = child.find("span", recursive=False)
                            if childElement:
                                child_a = childElement.find("a",
                                                            recursive=False)
                                if child_a:
                                    childLang = childElement.get("lang")
                                    childLink = child_a.get("href")

                                    if child_a:
                                        childWord = child_a.getText()
                                        if outputWord and childElement.getText(
                                        ) == child_a.getText():
                                            childID = parseWiktionaryPage(
                                                wiktionaryURL=childLink,
                                                rootLanguageCode=rootLanguageCode,
                                                parentLanguageCode=inputLanguageCode,
                                                inputLanguageCode=childLang,
                                                parentWord=outputWord,
                                                rootWord=rootWord,
                                                inputWord=childWord,
                                                inputMeaning=inputMeaning,
                                                derivedTerm=derivedWord,
                                                isBorrowed=isBorrowed,
                                                parentID=derivedID,
                                                rootID=rootID)
                                    else:
                                        childWord = childElement.getText()

                                    # parse descendants
                                    childSublist = child.find("ul")
                                    if childSublist:
                                        childDescendants = childSublist.find_all(
                                            "li", recursive=False)
                                        parseDescendantList(
                                            childDescendants, childLang,
                                            childWord, childID)

                            else:  # the child is possibly just introducing another list
                                childSublist = child.find("ul")
                                if childSublist:
                                    childDescendants = childSublist.find_all(
                                        "li", recursive=False)
                                    parseDescendantList(
                                        childDescendants, inputLanguageCode,
                                        outputWord, parentID)
            # print("here")
            # print(term)
            if isinstance(term, Tag) and term.find("ul", recursive=False):
                # print("also here")
                descendants = term.find("ul", recursive=False).find_all("li")
                # print("but not here")
                derivedID = derivedID if derivedID else 1
                parseDescendantList(
                    descendants, inputLanguageCode, derivedWord, derivedID)
            # print("we're here")

        return

    # the main event (well, one of them anyway)
    def parseDescendantList(descendants, parentLanguageCode, parentWord,
                            parentID):
        for descendant in descendants:
            # print("------------------------------------------")
            # print(f"next descendant: {descendant.getText()}")
            # print("------------------------------------------")

            descendantLang = parentLanguageCode
            descendantWord = parentWord
            descendantID = parentID

            if isinstance(descendant, Tag):
                # descendant_a = descendant.find("a")
                isBorrowed = False
                if descendant.find("span", recursive=False):
                    derivedByAdditionOfMorphemes = descendant.find(
                        "span",
                        attrs={"title": ["derived by addition of morphemes",
                                         "reshaped by analogy or addition of morphemes"]},
                        recursive=False)
                    if derivedByAdditionOfMorphemes is None:
                        if descendant.contents[0]:
                            if isinstance(descendant.contents[0], Tag):
                                isBorrowed = True if descendant.find(
                                    "span",
                                    attrs={"title": "borrowed"},
                                    recursive=False) else False
                            else:
                                # print(descendant.contents[0])
                                isBorrowed = True if descendant.find(
                                    "span",
                                    attrs={"title": "borrowed"},
                                    recursive=False) or descendant.contents[0][
                                        0] == "→" else False
                        elif descendant.contents:
                            isBorrowed = True if descendant.find(
                                "span",
                                attrs={"title": "borrowed"},
                                recursive=False
                            ) or descendant.contents[0] == "→" else False
                        else:
                            isBorrowed = True if descendant.find(
                                "span",
                                attrs={"title": "borrowed"},
                                recursive=False) else False
                    # do we have weirdness with the descendants actually being a list of derived terms?
                    if descendant.find("span", recursive=False):
                        if descendant.find("span", recursive=False).get(
                                "lang") == inputLanguageCode:
                            parseDerivedTerms(descendant, parentWord)

                    # try to find a link
                    descendant_a = None
                    descendantSpans = descendant.find_all(
                        "span",
                        attrs={"class": ["Latn", "Latinx"]},
                        recursive=False)
                    # try and get a link which leads somewhere if possible
                    for span in descendantSpans:
                        descendantLang = span.get("lang") if span.get(
                            "lang") else descendantLang
                        if span.find("a"):
                            descendant_a = span.find("a")
                            # get the link
                            # print(f"found a link: {descendant_a.get('href')}")
                            descendantLink = descendant_a.get("href")

                            try:
                                # print(descendant_a.parent)
                                # print("descendant_a.parent")
                                if descendant_a.parent.get("class") == [
                                        "Latn"
                                ] or descendant_a.parent.get("class") == [
                                        "Latinx"
                                ]:
                                    descendantWord = descendant_a.getText()
                                else:
                                    descendantWord = descendant.find(attrs={
                                        "class": ["Latinx", "Latn", "None"]
                                    }).getText()
                                # print("DescendantWord: " + descendantWord)
                                if descendant.find(
                                        attrs={"class": ["Latn", "Latinx"]}):
                                    # print('descendant.find(attrs={"class": ["Latn", "Latinx"]})')
                                    # print(descendant.find(attrs={"class": ["Latn", "Latinx"]}))
                                    descendantLang = descendant.find(attrs={
                                        "class": ["Latn", "Latinx"]
                                    }).get("lang") if descendant.find(attrs={
                                        "class": ["Latn", "Latinx"]
                                    }).get("lang") else descendantLang

                                    # if the descendantLang is the same as the inputLang, then we're dealing
                                    # with a derived term, so ABORT because wiktionary has displayed things
                                    # a bit incorrectly
                                    if descendantLang == inputLanguageCode:
                                        # print("you're at this point will")
                                        # print(descendant)
                                        parseDerivedTerms(
                                            descendant, parentWord)
                                    else:  # we're good, it's a real descendant
                                        # first parse the link
                                        if descendant_a.get("class"):
                                            if descendant_a.get("class")[0] != "new":
                                                descendantID = parseWiktionaryPage(
                                                    wiktionaryURL=descendantLink,
                                                    rootLanguageCode=rootLanguageCode,
                                                    parentLanguageCode=parentLanguageCode,
                                                    inputLanguageCode=descendantLang,
                                                    rootWord=rootWord,
                                                    parentWord=parentWord,
                                                    inputWord=descendantWord,
                                                    inputMeaning=outputMeaning,
                                                    derivedTerm=None,
                                                    isBorrowed=isBorrowed,
                                                    parentID=parentID,
                                                    rootID=rootID)
                                                if descendantID is None:
                                                    descendantID = insertWord({
                                                        "lang":
                                                        descendantLang,
                                                        "parentLang":
                                                        parentLanguageCode,
                                                        "rootLang":
                                                        rootLanguageCode,
                                                        "latinTranscription":
                                                        descendantWord,
                                                        "parentLatinTranscription":
                                                        parentWord,
                                                        "rootLatinTranscription":
                                                        rootWord,
                                                        "ipaTranscription":
                                                        ipaTranscription,
                                                        "meaning":
                                                        outputMeaning,
                                                        "url":
                                                        descendantLink,
                                                        "derivedRoot":
                                                        derivedTerm,
                                                        "isBorrowed":
                                                        isBorrowed,
                                                        "tags": [],
                                                        "soundChanges": [],
                                                        "parentID":
                                                        parentID,
                                                        "rootID":
                                                        rootID,
                                                        "linkType": "PARENT"
                                                    })
                                            else:  # link is red
                                                descendantID = insertWord({
                                                    "lang":
                                                    descendantLang,
                                                    "parentLang":
                                                    parentLanguageCode,
                                                    "rootLang":
                                                    rootLanguageCode,
                                                    "latinTranscription":
                                                    descendantWord,
                                                    "parentLatinTranscription":
                                                    parentWord,
                                                    "rootLatinTranscription":
                                                    rootWord,
                                                    "ipaTranscription":
                                                    ipaTranscription,
                                                    "meaning":
                                                    outputMeaning,
                                                    "url":
                                                    descendantLink,
                                                    "derivedRoot":
                                                    derivedTerm,
                                                    "isBorrowed":
                                                    isBorrowed,
                                                    "tags": [],
                                                    "soundChanges": [],
                                                    "parentID":
                                                    parentID,
                                                    "rootID":
                                                    rootID,
                                                    "linkType": "PARENT"
                                                })
                                        else:
                                            descendantID = parseWiktionaryPage(
                                                wiktionaryURL=descendantLink,
                                                rootLanguageCode=rootLanguageCode,
                                                parentLanguageCode=parentLanguageCode,
                                                inputLanguageCode=descendantLang,
                                                rootWord=rootWord,
                                                parentWord=parentWord,
                                                inputWord=descendantWord,
                                                inputMeaning=outputMeaning,
                                                derivedTerm=None,
                                                isBorrowed=isBorrowed,
                                                parentID=parentID,
                                                rootID=rootID)

                                        # next parse any sublists
                                        if (descendant.find("ul",
                                                            recursive=False)):
                                            print(
                                                f"printing sublist of {descendantWord}")
                                            parseDescendantList(
                                                descendant.find(
                                                    "ul", recursive=False),
                                                descendantLang, descendantWord,
                                                descendantID)
                            except:
                                print("-------------ERROR-------------")
                                PrintException()
                                pass
                        else:  # else no link
                            descendantWord = span.getText()
                            descendantID = insertWord({
                                "lang":
                                descendantLang,
                                "parentLang":
                                parentLanguageCode,
                                "rootLang":
                                rootLanguageCode,
                                "latinTranscription":
                                descendantWord,
                                "parentLatinTranscription":
                                parentWord,
                                "rootLatinTranscription":
                                rootWord,
                                "ipaTranscription":
                                ipaTranscription,
                                "meaning":
                                outputMeaning,
                                "url":
                                wiktionaryURL,
                                "derivedRoot":
                                derivedTerm,
                                "isBorrowed":
                                isBorrowed,
                                "tags": [],
                                "soundChanges": [],
                                "parentID":
                                parentID,
                                "rootID":
                                rootID,
                                "linkType": "PARENT"
                            })
                            # next parse any sublists
                            if (descendant.find("ul", recursive=False)):
                                parseDescendantList(
                                    descendant.find("ul", recursive=False),
                                    descendantLang, descendantWord,
                                    descendantID)

                    # if descendant_a and descendant_a != -1:
                    # print("descendant_a")
                    # print(descendant_a)
                else:  # the child is possibly just introducing another list
                    childSublist = descendant.find("ul")
                    if childSublist:
                        childDescendants = childSublist.find_all(
                            "li", recursive=False)
                        parseDescendantList(childDescendants,
                                            parentLanguageCode, parentWord,
                                            parentID)

    linkType = "PARENT" if derivedTerm is None else "DERIVED"

    # first find all transliterated headwords in the right lang
    headword = None
    headwordList = searchResponse.find_all(attrs={
        "class": "headword-tr",
        "lang": [inputLanguageCode, inputLanguageCode+"-Latn"]
    })

    # iterate through list until we find one that matches our input word
    # (diacritics are dicks)
    for word in headwordList:
        if word.getText() == inputWord or word.get("lang") != inputLanguageCode:
            outputWord = word
            headword = word
            break
        # while isinstance(outputWord, Tag):
        #     outputWord = outputWord.contents[0]
        #     if headword is None:
        #         headword = word
        #         if outputWord == inputWord:
        #             headword = word
        #             break

    if len(headwordList
           ) == 0:  # , then the word probably naturally appears in latin script
        # find headword in latin script
        headwordList = searchResponse.find_all(attrs={
            "class": ["headword"],
            "lang": inputLanguageCode
        })

        # iterate through list until we find one that matches our input word
        # (diacritics are dicks)
        for word in headwordList:
            # print("word.getText():")
            # print(word.getText())
            # print("inputWord:")
            # print(inputWord)
            if word.getText() == inputWord:
                outputWord = word
                headword = word
                break
            # outputWord = word
            # while isinstance(outputWord, Tag):
            #     outputWord = outputWord.contents[0]
            #     if headword is None:
            #         headword = word
            #         if outputWord == inputWord:
            #             headword = word
            #             break

    # should always be true, so probably don't need this
    if headword:
        outputWord = headword
        while isinstance(outputWord, Tag):
            # set the word we're inserting (its latin transcription)
            outputWord = outputWord.getText()

        # let's try and find the meaning
        if headword.find_next("li"):
            if headword.find_next("li") != headword.find_next(
                    "li", attrs={"class": "mw-empty-elt"}):
                meaning = headword.find_next("li").getText()

                # the meaning might contain multiple ways of explaining it.
                # we only want the first one
                commaIndex = meaning.find(",")
                semicolonIndex = meaning.find(";")
                if commaIndex != -1 and semicolonIndex != -1:
                    splitIndex = min(commaIndex, semicolonIndex)
                elif commaIndex == -1 and semicolonIndex != -1:
                    splitIndex = semicolonIndex
                elif commaIndex != -1 and semicolonIndex == -1:
                    splitIndex = commaIndex
                else:
                    splitIndex = -1
                # if there's a splitter, only take the string up to that point
                outputMeaning = meaning[
                    0:splitIndex] if splitIndex != -1 else meaning

            else:
                meaning = headword.find_next("li").find_next("li").getText()
                commaIndex = meaning.find(",")
                semicolonIndex = meaning.find(";")
                if commaIndex != -1 and semicolonIndex != -1:
                    splitIndex = min(commaIndex, semicolonIndex)
                elif commaIndex == -1 and semicolonIndex != -1:
                    splitIndex = semicolonIndex
                elif commaIndex != -1 and semicolonIndex == -1:
                    splitIndex = commaIndex
                else:
                    splitIndex = -1
                outputMeaning = meaning[
                    0:splitIndex] if splitIndex != -1 else meaning
        else:  # if we can't find a new meaning, assume the meaning hasn't changed
            outputMeaning = inputMeaning

        # if we're on the root code, pieWord is the headword
        if inputLanguageCode == rootLanguageCode:
            pieWord = outputWord

        # find list of descendants

        # find next headword of a different language
        next_headword = headword.find_next(
            attrs={"class": ["headword", "headword-tr"]})
        while next_headword:
            if next_headword.get("lang") == inputLanguageCode or (
                    next_headword.get("lang") + "-Latn") == inputLanguageCode:
                next_headword = next_headword.find_next(
                    attrs={"class": ["headword", "headword-tr"]})
            else:
                break

        # find IPA Transcription of headword
        try:
            if headword.find_previous(
                    attrs={"class": ["headword", "headword-tr"]}):
                if headword.find_previous(
                        attrs={"class": "IPA"}) != headword.find_previous(
                            attrs={
                                "class": ["headword", "headword-tr"]
                            }).find_previous(attrs={"class": "IPA"}):
                    ipaTranscription = headword.find_previous(attrs={
                        "class": "IPA"
                    }).find_parent("ul").find(attrs={
                        "class": "IPA"
                    }).contents[0]
                    # print(ipaTranscription)
            else:
                ipaTranscription = None

        except Exception as e:
            print(e)
            ipaTranscription = None
            pass

        # sometimes for the root word, the word appears
        # differently on the page compared to the lemma
        # list (usually because of accents)
        if parentLanguageCode == "NONE":
            rootWord = outputWord

        parentID = insertWord({
            "lang": inputLanguageCode,
            "parentLang": parentLanguageCode,
            "rootLang": rootLanguageCode,
            "latinTranscription": outputWord,
            "parentLatinTranscription": parentWord,
            "rootLatinTranscription": rootWord,
            "ipaTranscription": ipaTranscription,
            "meaning": outputMeaning,
            "url": wiktionaryURL,
            "derivedRoot": derivedTerm,
            "isBorrowed": isBorrowed,
            "tags": [],
            "soundChanges": [],
            "parentID": parentID,
            "rootID": rootID,
            "linkType": linkType
        })

        # first if there's a next headword (i.e. other languages on that page)
        if next_headword:
            # only process descendants if they belong to the correct language
            if headword.find_next(
                    attrs={
                        "id": [
                            "Descendants", "Descendants_2", "Descendants_3",
                            "Descendants_4", "Descendants_5", "Descendants_6"
                        ]
                    }) != next_headword.find_next(
                        attrs={
                            "id": [
                                "Descendants", "Descendants_2",
                                "Descendants_3", "Descendants_4",
                                "Descendants_5", "Descendants_6"
                            ]
                        }):
                descendantHeader = headword.find_next(
                    attrs={
                        "id": [
                            "Descendants", "Descendants_2", "Descendants_3",
                            "Descendants_4", "Descendants_5", "Descendants_6"
                        ]
                    })
                descendantList = descendantHeader.find_next("ul")
                descendantItems = descendantList.find_all("li",
                                                          recursive=False)
                parseDescendantList(descendantItems, inputLanguageCode,
                                    outputWord, parentID)
                try:
                    while descendantList.next_sibling.next_sibling.name == "ul":
                        # print("got here (1)")
                        descendantList = descendantList.next_sibling.next_sibling
                        descendantItems = descendantList.find_all(
                            "li", recursive=False)
                        parseDescendantList(descendantItems, inputLanguageCode,
                                            outputWord, parentID)
                except:  # no more siblings
                    pass
            # only process derived terms if they belong to the correct language
            if headword.find_next(
                    attrs={"id": re.compile(r'Derived_terms(_\d+)?')}
            ) != headword.find_next(attrs={
                    "class": ["headword", "headword-tr"]
            }).find_next(attrs={"id": re.compile(r'Derived_terms(_\d+)?')
                                }):
                derivedTermsHeader = headword.find_next(
                    attrs={"id": re.compile(r'Derived_terms(_\d+)?')})
                # print("------------------------------------------------------")
                # print("DERIVED TERMS HEADER")
                # print("------------------------------------------------------")
                if derivedTermsHeader:
                    derivedTermsList = derivedTermsHeader.find_next(
                        "ul").find_all("li", recursive=False)
                    parseDerivedTerms(derivedTermsList, outputWord)

    # if there are no more languages on that page, things are easier
        elif next_headword is None:
            descendantHeader = headword.find_next(
                attrs={
                    "id": [
                        "Descendants", "Descendants_2", "Descendants_3",
                        "Descendants_4", "Descendants_5", "Descendants_6"
                    ]
                })
            if descendantHeader:
                descendantList = descendantHeader.find_next("ul")
                descendantItems = descendantList.find_all("li",
                                                          recursive=False)
                parseDescendantList(descendantItems, inputLanguageCode,
                                    outputWord, parentID)
                try:
                    while descendantList.next_sibling.next_sibling.name == "ul":
                        # print("got here (2)")
                        descendantList = descendantList.next_sibling.next_sibling
                        descendantItems = descendantList.find_all(
                            "li", recursive=False)
                        parseDescendantList(descendantItems, inputLanguageCode,
                                            outputWord, parentID)
                except:  # no more siblings
                    pass
            derivedTermsHeader = headword.find_next(
                attrs={"id": re.compile(r'Derived_terms(_\d+)?')})
            # print("------------------------------------------------------")
            # print("DERIVED TERMS HEADER")
            # print("------------------------------------------------------")
            # print(derivedTermsHeader)
            if (derivedTermsHeader and inputLanguageCode == rootLanguageCode):
                derivedTermsList = derivedTermsHeader.find_next("ul").find_all(
                    "li", recursive=False)
                parseDerivedTerms(derivedTermsList, outputWord)

    else:  # otherwise language does not exist on that page (we were duped!)
        # but luckily we have enough information from the input vars
        outputWord = inputWord
        ipaTranscription = None
        outputMeaning = inputMeaning
        parentID = insertWord({
            "lang": inputLanguageCode,
            "parentLang": parentLanguageCode,
            "rootLang": rootLanguageCode,
            "latinTranscription": outputWord,
            "parentLatinTranscription": parentWord,
            "rootLatinTranscription": rootWord,
            "ipaTranscription": ipaTranscription,
            "meaning": outputMeaning,
            "url": wiktionaryURL,
            "derivedRoot": derivedTerm,
            "isBorrowed": isBorrowed,
            "tags": [],
            "soundChanges": [],
            "parentID": parentID,
            "rootID": rootID,
            "linkType": linkType
        })

    searchResponse.decompose()
    return parentID


# lemmaURL = "https://en.m.wiktionary.org/w/index.php?title=Category:Proto-Indo-European_lemmas&pagefrom=H3ER-%0AProto-Indo-European%2Fh%E2%82%83er-#mw-pages"
# lemmaURL = "https://en.m.wiktionary.org/w/index.php?title=Category:Proto-Indo-European_lemmas&pagefrom=MEH2D-%0AProto-Indo-European%2Fmeh%E2%82%82d-#mw-pages"
# lemmaURL = "https://en.m.wiktionary.org/w/index.php?title=Category:Proto-Indo-European_lemmas&pagefrom=SEM%0AProto-Indo-European%2Fs%E1%B8%97m#mw-pages"
# lemmaURL = "https://en.wiktionary.org/wiki/Reconstruction:Proto-Indo-Iranian/d%CA%B0%C3%A1%C7%B0%CA%B0ati"
# iteration=0
# parseWiktionaryPage("/wiki/Reconstruction:Proto-Indo-European/bʰer-", "NONE", "ine-pro", "NONE", None, None, None, None)
# time.sleep(2000)

pieLangCode = "ine-pro"
uralicLangCode = "urj-pro"
semiticLangCode = "sem-pro"
austronesianLangCode = "map-pro"
trkLangCode = "trk-pro"

# lemmaURL = "https://en.wiktionary.org/wiki/Category:Proto-Uralic_lemmas"
# lemmaURL = "https://en.wiktionary.org/wiki/Category:Proto-Austronesian_lemmas"
# lemmaURL = "https://en.wiktionary.org/wiki/Category:Proto-Semitic_lemmas"
# lemmaURL = "https://en.wiktionary.org/wiki/Category:Proto-Turkic_lemmas"
# lemmaURL = "https://en.wiktionary.org/w/index.php?title=Category:Proto-Indo-European_lemmas&pagefrom=G%27ENUS%0AProto-Indo-European%2F%C7%B5%C3%A9nus#mw-pages"
# lemmaURL = "https://en.wiktionary.org/w/index.php?title=Category:Proto-Indo-European_lemmas&pagefrom=H2ELYOS%0AProto-Indo-European%2Fh%E2%82%82%C3%A9lyos#mw-pages"
# lemmaURL = "https://en.wiktionary.org/w/index.php?title=Category:Proto-Indo-European_lemmas&pagefrom=KEH3-%0AProto-Indo-European%2Fkeh%E2%82%83-#mw-pages"

# tempurl
# lemmaURL = "https://en.wiktionary.org/w/index.php?title=Category:Proto-Indo-European_lemmas&pagefrom=p#mw-pages"

# print(lemma_searchResponse)
# print(lemma_searchResponse.find("div", attrs={"class": "mw-content-ltr"}))

# iteration = 0
if testMode == 1:
    parseWiktionaryPage(wiktionaryURL=testURL,
                        rootLanguageCode=testLang,
                        parentLanguageCode="NONE",
                        inputLanguageCode=testLang,
                        rootWord=testWord,
                        parentWord=None,
                        inputWord=testWord,
                        inputMeaning=None,
                        derivedTerm=None,
                        isBorrowed=False,
                        parentID=None,
                        rootID=uuid.uuid4())
    print("inserted test word")
else:

    def parseLemmas(url):
        global iteration
        lemma_request_object = requests.get(url)
        lemma_searchResponse = BeautifulSoup(
            lemma_request_object.content, 'html.parser')
        # print(lemma_searchResponse)
        lemmas = lemma_searchResponse.find(
            attrs={"id": "mw-pages"}).find_all("li")
        for lemma in lemmas:
            print(lemma.find("a").contents[0])
            if lemma.find("a").contents[0].find("/") != -1:
                rootWord = "*" + \
                    re.split("/", lemma.find("a").contents[0], 1)[1]
            else:
                rootWord = lemma.find("a").contents[0]

            lemmaHref = lemma.find("a").get("href")
            iteration = 0
            parseWiktionaryPage(wiktionaryURL=lemmaHref,
                                rootLanguageCode=rootLang,
                                parentLanguageCode="NONE",
                                inputLanguageCode=rootLang,
                                rootWord=rootWord,
                                parentWord=None,
                                inputWord=rootWord,
                                inputMeaning=None,
                                derivedTerm=None,
                                isBorrowed=False,
                                parentID=None,
                                rootID=uuid.uuid4())

    ########################
    # PIE
    ########################
    # rootLang = pieLangCode
    # lemmaURL = "https://en.m.wiktionary.org/w/index.php?title=Category:Proto-Indo-European_lemmas"
    # parseLemmas(lemmaURL)
    # lemmaURL = "https://en.m.wiktionary.org/w/index.php?title=Category:Proto-Indo-European_lemmas&pagefrom=G%27ENH1TRIH2%0AProto-Indo-European%2F%C7%B5%C3%A9nh%E2%82%81trih%E2%82%82#mw-pages"
    # parseLemmas(lemmaURL)
    # lemmaURL = "https://en.m.wiktionary.org/w/index.php?title=Category:Proto-Indo-European_lemmas&pagefrom=H2ELTEROS%0AProto-Indo-European%2Fh%E2%82%82%C3%A9lteros#mw-pages"
    # parseLemmas(lemmaURL)
    # lemmaURL = "https://en.m.wiktionary.org/w/index.php?title=Category:Proto-Indo-European_lemmas&pagefrom=KEH2N-%0AProto-Indo-European%2Fkeh%E2%82%82n-#mw-pages"
    # parseLemmas(lemmaURL)
    # lemmaURL = "https://en.m.wiktionary.org/w/index.php?title=Category:Proto-Indo-European_lemmas&pagefrom=PELH2-%0AProto-Indo-European%2Fpelh%E2%82%82-#mw-pages"
    # parseLemmas(lemmaURL)
    # lemmaURL = "https://en.m.wiktionary.org/w/index.php?title=Category:Proto-Indo-European_lemmas&pagefrom=STEH2YETI%0AProto-Indo-European%2Fst%C3%A9h%E2%82%82yeti#mw-pages"
    # parseLemmas(lemmaURL)
    # lemmaURL = "https://en.m.wiktionary.org/w/index.php?title=Category:Proto-Indo-European_lemmas&pagefrom=WOK%27EH2%0AProto-Indo-European%2Fwo%E1%B8%B1%C3%A9h%E2%82%82#mw-pages"
    # parseLemmas(lemmaURL)

    #######################
    # LATIN
    #######################
    rootLang = "cel-pro"
    rootLemmaURL = "https://en.wiktionary.org/wiki/Category:Proto-Celtic_lemmas"
    parseLemmas(rootLemmaURL)

    rootLang = "iir-pro"
    rootLemmaURL = "https://en.wiktionary.org/wiki/Category:Proto-Indo-Iranian_lemmas"
    parseLemmas(rootLemmaURL)
    # rootLemmaURL = "https://en.wiktionary.org/wiki/Category:Proto-Germanic_lemmas"
    # rootLemmaURL = "https://en.wiktionary.org/wiki/Category:Proto-Indo-European_lemmas"
    # rootLemmaURL = "https://en.wiktionary.org/w/index.php?title=Category:Latin_lemmas&from=ne"
    # rootLemmaURL = "https://en.wiktionary.org/w/index.php?title=Category:Ancient_Greek_lemmas"

    # get the root lemma page
    request_object = requests.get(rootLemmaURL)
    searchResponse = BeautifulSoup(request_object.content, 'html.parser')
    # is there a next page?
    lemma_links = searchResponse.find_all("a")
    next_page_link = ""
    for link in lemma_links:
        print(link)
        if link.contents and link.contents[0] == "next page":
            print("FOUND A BLOODY LINK")
            next_page_link = link.get("href")
            print("next_page_link")
            print(next_page_link)
            break

    while next_page_link != "":
        lemma_request_object = requests.get(rootURL + next_page_link)
        lemma_response = BeautifulSoup(lemma_request_object.content,
                                       'html.parser')
        lemma_links = lemma_response.find_all("a")
        next_page_link = ""
        for link in lemma_links:
            if link.contents and link.contents[0] == "next page":
                next_page_link = link.get("href")
                print("href")
                print(link.get("href"))
                break
        print(rootURL + next_page_link)
        parseLemmas(rootURL + next_page_link)

    ########################
    # PROTO-GERMANIC
    ########################
    rootLang = "gem-pro"
    # lemmaURL = "https://en.wiktionary.org/wiki/Category:Proto-Germanic_lemmas"
    # parseLemmas(lemmaURL)
    # lemmaURL = "https://en.wiktionary.org/w/index.php?title=Category:Proto-Germanic_lemmas&pagefrom=ANDAWLITJAN%0AProto-Germanic%2Fandawlitją#mw-pages"
    # parseLemmas(lemmaURL)
    # lemmaURL = "https://en.wiktionary.org/w/index.php?title=Category:Proto-Germanic_lemmas&pagefrom=BARWAZ%0AProto-Germanic%2Fbarwaz#mw-pages"
    # parseLemmas(lemmaURL)
    # lemmaURL = "https://en.wiktionary.org/w/index.php?title=Category:Proto-Germanic_lemmas&pagefrom=DIMMAZ%0AProto-Germanic%2Fdimmaz#mw-pages"
    # parseLemmas(lemmaURL)
    # lemmaURL = "https://en.wiktionary.org/w/index.php?title=Category:Proto-Germanic_lemmas&pagefrom=FAITIN%0AProto-Germanic%2Ffait%C4%AF%CC%84#mw-pages"
    # parseLemmas(lemmaURL)

    # lemmaURL = "https://en.m.wiktionary.org/w/index.php?title=Category:Proto-Indo-European_lemmas&pagefrom=W#mw-pages"
    # parseLemmas(lemmaURL)

    # rootLang = semiticLangCode

    # lemmaURL = "https://en.m.wiktionary.org/wiki/Category:Proto-Semitic_lemmas"
    # parseLemmas(lemmaURL)

    # rootLang = uralicLangCode

    # lemmaURL = "https://en.m.wiktionary.org/wiki/Category:Proto-Uralic_lemmas"
    # parseLemmas(lemmaURL)
    # lemmaURL = "https://en.m.wiktionary.org/w/index.php?title=Category:Proto-Uralic_lemmas&pagefrom=T%C3%84%0AProto-Uralic%2Ft%C3%A4#mw-pages"
    # parseLemmas(lemmaURL)

    # rootLang="dra-pro"
    # lemmaURL="https://en.m.wiktionary.org/wiki/Category:Proto-Dravidian_lemmas"
    # parseLemmas(lemmaURL)

    # rootLang="bnt-pro"
    # lemmaURL ="https://en.m.wiktionary.org/wiki/Category:Proto-Bantu_lemmas"
    # parseLemmas(lemmaURL)

    # rootLang = austronesianLangCode
    # lemmaURL = "https://en.m.wiktionary.org/wiki/Category:Proto-Austronesian_lemmas"
    # parseLemmas(lemmaURL)

    # rootLang="jpx-pro"
    # lemmaURL="https://en.m.wiktionary.org/wiki/Category:Proto-Japonic_lemmas"
    # parseLemmas(lemmaURL)

    # rootLang=trkLangCode
    # lemmaURL="https://en.m.wiktionary.org/wiki/Category:Proto-Turkic_lemmas"
    # parseLemmas(lemmaURL)

    # rootLang="mkh-pro"
    # lemmaURL="https://en.m.wiktionary.org/wiki/Category:Proto-Mon-Khmer_lemmas"
    # parseLemmas(lemmaURL)

    # rootLang="xgn-pro"
    # lemmaURL="https://en.m.wiktionary.org/wiki/Category:Proto-Mongolic_lemmas"
    # parseLemmas(lemmaURL)

    print("DONE WITH INSERT, CHECK IT")
