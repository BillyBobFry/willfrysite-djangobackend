from neo4j import GraphDatabase
import json
import time
from datetime import datetime
import array
import string
import re
from pymongo import MongoClient

client = MongoClient('localhost', 27017)
db = client.cache
collection = db["etymologyWords"]

# empty the collection
collection.drop()

uri = "bolt://localhost:7687"
driver = GraphDatabase.driver(uri, auth=("neo4j", "pass"), encrypted=False)


roots=[]

for letter1 in string.ascii_lowercase[:26]:
    for letter2 in string.ascii_lowercase[:26]:
        thisRoot = letter1+letter2
        # print(letter1 + letter2)
        # roots.append(thisRoot)
        searchTerm = thisRoot
        languageCode = "en"
        returnCompounds = False

        def getMatchingWords(tx):
            results = tx.run(
                "match (a)"
                " where toLower(a.latinTranscription) starts with $searchTerm"
                " and toLower(a.languageCode)=toLower($languageCode)"
                f''' {' ' if returnCompounds else ' and NOT (a.latinTranscription contains " " or a.latinTranscription contains "-")' }'''
                " return a",
                languageCode=languageCode, searchTerm=searchTerm
            )

            graph = {}
            nodes = list(map(lambda e: e.id, results.graph().nodes))
            for node in nodes:
                graph[node] = []

            def getObj(obj):
                o = {key: obj.get(key) for key in list(obj.keys())}
                if len(o) > 0:
                    o.update({"id": obj.id})
                    return o
                else:
                    return None

            nodes = list(filter(lambda e: e, map(getObj, results.graph().nodes)))
            return({"nodes": nodes})

        with driver.session() as session:
            cachedTerm = session.read_transaction(getMatchingWords)
            cachedTerm["searchTerm"] = searchTerm
            cachedTerm["languageCode"] = languageCode
            cachedTerm["count"] = 0
            cachedTerm["lastDate"] = datetime.now()
            collection.insert(cachedTerm)
            if len(cachedTerm["nodes"]) > 0:
                roots.append(cachedTerm)


print(roots)