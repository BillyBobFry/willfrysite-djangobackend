from neo4j import GraphDatabase
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import json
import time
import datetime
import array
import requests
import re
from pymongo import MongoClient

client = MongoClient('localhost', 27017)
linguisticsDB = client.linguistics

MongoClient = MongoClient('localhost', 27017)
cacheDB = MongoClient.cache
freetextCache = cacheDB["etymologyWords"]


uri = "bolt://localhost:7687"
try:
    driver = GraphDatabase.driver(uri, auth=("neo4j", "pass"), encrypted=False)
except:  # if neo4j isn't contactible we still want to run the rest of the site
    pass


def index(request):

    def print_friends_of(tx, name):
        results = tx.run("MATCH (a:word)-[r]->(b:word)"
                         "RETURN a, r, b", name=name)

        graph = {}
        nodes = list(map(lambda e: e.id, results.graph().nodes))
        for node in nodes:
            graph[node] = []

        relationships = results.graph().relationships
        for relationship in relationships:
            nodesInRelationship = list(map(lambda e: e.id, relationship.nodes))
            graph[nodesInRelationship[0]].append(nodesInRelationship[1])

        def getObj(obj):
            o = {key: obj.get(key) for key in list(obj.keys())}
            o.update({"id": obj.id})
            return o

        links = list(map(lambda e: {"source": e[0].id, "target": e[1].id}, map(
            lambda e: list(e.nodes), results.graph().relationships)))
        return({"nodes": list(map(getObj, results.graph().nodes)),
                "links": links})

    with driver.session() as session:
        resp = session.read_transaction(print_friends_of, "elf")
        # print(resp)
        return JsonResponse(resp)


def getAllWords(request):
    def allWords(tx, name):
        results = tx.run("MATCH (a:word)-[r]->(b:word)"
                         "RETURN a, r, b", name=name)

        graph = {}
        nodes = list(map(lambda e: e.id, results.graph().nodes))
        for node in nodes:
            graph[node] = []

        relationships = results.graph().relationships
        for relationship in relationships:
            nodesInRelationship = list(map(lambda e: e.id, relationship.nodes))
            graph[nodesInRelationship[0]].append(nodesInRelationship[1])

        def getObj(obj):
            o = {key: obj.get(key) for key in list(obj.keys())}
            o.update({"id": obj.id})
            return o

        links = list(map(lambda e: {"source": e[0].id, "target": e[1].id, "type": e[2]}, map(
            lambda e: list(e.nodes) + [e.type], results.graph().relationships)))
        return({"nodes": list(map(getObj, results.graph().nodes)),
                "links": links})

    with driver.session() as session:
        resp = session.read_transaction(allWords, "elf")
        return JsonResponse(resp)


def getRootWord(request):
    rootWord = {'languageCode': 'ine-pro',
                'languageName': 'Proto-Indo-European', 'latinTranscription': '*wódr̥'}

    def getRootWordFromGraph(tx, word):
        results = tx.run(
            f"match (a:word {{languageName:\'{rootWord['languageName']}\', languageCode:\'{rootWord['languageCode']}\', latinTranscription:\'{rootWord['latinTranscription']}\'}})-[r]->(b) return a,b,r")

        graph = {}
        nodes = list(map(lambda e: e.id, results.graph().nodes))
        for node in nodes:
            graph[node] = []

        relationships = results.graph().relationships
        for relationship in relationships:
            nodesInRelationship = list(map(lambda e: e.id, relationship.nodes))
            graph[nodesInRelationship[0]].append(nodesInRelationship[1])

        def getObj(obj):
            o = {key: obj.get(key) for key in list(obj.keys())}
            o.update({"id": obj.id})
            return o

        links = list(map(lambda e: {"source": e[0].id, "target": e[1].id, "type": e[2]}, map(
            lambda e: list(e.nodes) + [e.type], results.graph().relationships)))
        return({"nodes": list(map(getObj, results.graph().nodes)),
                "links": links})

    with driver.session() as session:
        resp = session.read_transaction(getRootWordFromGraph, rootWord)
        return JsonResponse(resp)
    return JsonResponse({"1": "2"})


def parseGraph(graph):
    return True


def addWord(request):
    inputNode = json.loads(request.body)['node']

    def getSingleWordFromGraph(tx):
        results = tx.run(
            f"match (a) "
            f" where a.languageCode = '{inputNode['languageCode']}'"
            f" and a.languageName = '{inputNode['languageName']}'"
            f" and a.latinTranscription = '{inputNode['latinTranscription']}'"
            f" return a"
        )

        graph = {}
        nodes = list(map(lambda e: e.id, results.graph().nodes))
        for node in nodes:
            graph[node] = []

        relationships = results.graph().relationships
        for relationship in relationships:
            nodesInRelationship = list(map(lambda e: e.id, relationship.nodes))
            graph[nodesInRelationship[0]].append(nodesInRelationship[1])

        def getObj(obj):
            o = {key: obj.get(key) for key in list(obj.keys())}
            if len(o) > 0:
                o.update({"id": obj.id})
                return o
            else:
                return None

        links = list(map(lambda e: {"source": e[0].id, "target": e[1].id, "type": e[2]}, map(
            lambda e: list(e.nodes) + [e.type], results.graph().relationships)))
        nodes = list(filter(lambda e: e, map(getObj, results.graph().nodes)))
        return({"nodes": nodes,
                "links": links})

    with driver.session() as session:
        resp = session.read_transaction(getSingleWordFromGraph)
        return JsonResponse(resp)
    return JsonResponse({"1": "2"})


def autocomplete(request):
    searchTerm = json.loads(request.body)['searchTerm'].lower()
    languageName = json.loads(request.body)['language']
    languageCode = getLanguageCodeForLanguageName(languageName)
    returnCompounds = json.loads(request.body)['returnCompoundTerms']

    def getMatchingWords(tx):
        results = tx.run(
            "match (a)"
            " where toLower(a.latinTranscription) starts with $searchTerm"
            " and toLower(a.languageCode)=toLower($languageCode)"
            f''' {' ' if returnCompounds else ' and NOT (a.latinTranscription contains " " or a.latinTranscription contains "-")' }'''
            " return a",
            languageCode=languageCode, searchTerm=searchTerm
        )

        graph = {}
        nodes = list(map(lambda e: e.id, results.graph().nodes))
        for node in nodes:
            graph[node] = []

        def getObj(obj):
            o = {key: obj.get(key) for key in list(obj.keys())}
            if len(o) > 0:
                o.update({"id": obj.id})
                return o
            else:
                return None

        nodes = list(filter(lambda e: e, map(getObj, results.graph().nodes)))
        return({"nodes": nodes})

    # first check the cache
    firstTwoCharacters = searchTerm[0:2]
    cachedResult = list(freetextCache.find({"searchTerm": firstTwoCharacters, "languageCode": languageCode}, {"_id": 0}))
    
    # if we've got a cached result, use it
    if len(cachedResult) > 0:
        cachedResult = cachedResult[0]

        # update the cache
        newDate = datetime.datetime.now()
        freetextCache.update({"searchTerm": firstTwoCharacters, "languageCode": languageCode},{"$set": {"lastDate": newDate}, "$inc": {"count": 1}})

        # get all cached nodes that start with our string
        matchingTerms = filter(lambda e: e["latinTranscription"][0: len(searchTerm)].lower() == searchTerm.lower(), cachedResult["nodes"])
        cachedResult["nodes"] = list(matchingTerms)
        cachedResult["searchTerm"] = searchTerm

        return JsonResponse(cachedResult)
    else:
        with driver.session() as session:
            resp = session.read_transaction(getMatchingWords)

            # insert into cache
            cachedObject = resp
            cachedObject["searchTerm"] = firstTwoCharacters
            cachedObject["languageCode"] = languageCode
            cachedObject["lastDate"] = datetime.datetime.now()
            cachedObject["count"] = 1
            freetextCache.insert(cachedObject)

            resp["searchTerm"] = searchTerm
            try:
                del resp["_id"]
            except KeyError:
                pass

            return JsonResponse(resp)


def getDoublets(request):
    inputNode = json.loads(request.body)['node']
    returnCompounds = json.loads(request.body)['returnCompoundTerms']

    def getDoubletsFromGraph(tx):
        results = tx.run(
            f"match p=(a)<-[q *1..100]-(b)-[r *0..100]->(d)-->(c) "
            " where a.languageCode=$languageCode"
            " and c.languageCode=$languageCode"
            " and d.languageCode <> c.languageCode"
            f" and c.latinTranscription <> a.latinTranscription"
            " and a.languageName=$languageName"
            " and a.latinTranscription = $latinTranscription"
            f''' {' ' if returnCompounds else ' and NOT (c.latinTranscription contains " " or c.latinTranscription contains "-")' }'''
            f" return relationships(p), nodes(p)",
            languageCode=inputNode['languageCode'], languageName=inputNode[
                'languageName'], latinTranscription=inputNode['latinTranscription']
        )

        graph = {}
        nodes = list(map(lambda e: e.id, results.graph().nodes))
        for node in nodes:
            graph[node] = []

        relationships = results.graph().relationships
        for relationship in relationships:
            nodesInRelationship = list(map(lambda e: e.id, relationship.nodes))
            graph[nodesInRelationship[0]].append(nodesInRelationship[1])

        def getObj(obj):
            o = {key: obj.get(key) for key in list(obj.keys())}
            if len(o) > 0:
                o.update({"id": obj.id})
                return o
            else:
                return None

        links = list(map(lambda e: {"source": e[0].id, "target": e[1].id, "type": e[2]}, map(
            lambda e: list(e.nodes) + [e.type], results.graph().relationships)))
        nodes = list(filter(lambda e: e, map(getObj, results.graph().nodes)))
        return({"nodes": nodes,
                "links": links})

    with driver.session() as session:
        resp = session.read_transaction(getDoubletsFromGraph)
        return JsonResponse(resp)


def getEtymology(request):
    inputNode = json.loads(request.body)['node']

    def getParentFromGraph(tx):
        # print(statement)
        results = tx.run(
            "match (a)-[r *1..100]->(b) where b.languageCode = $languageCode"
            " and b.languageName = $languageName"
            " and b.latinTranscription = $latinTranscription"
            " return a,r",
            {"languageCode": inputNode['languageCode'], "languageName": inputNode[
                'languageName'], "latinTranscription": inputNode['latinTranscription']}
        )

        graph = {}
        nodes = list(map(lambda e: e.id, results.graph().nodes))
        for node in nodes:
            graph[node] = []

        relationships = results.graph().relationships
        for relationship in relationships:
            nodesInRelationship = list(map(lambda e: e.id, relationship.nodes))
            graph[nodesInRelationship[0]].append(nodesInRelationship[1])

        def getObj(obj):
            o = {key: obj.get(key) for key in list(obj.keys())}
            if len(o) > 0:
                o.update({"id": obj.id})
                return o
            else:
                return None
        print(list(results.graph().relationships))
        links = list(map(lambda e: {"source": e[0].id, "target": e[1].id, "type": e[2]}, map(
            lambda e: list(e.nodes) + [e.type], results.graph().relationships)))
        nodes = list(filter(lambda e: e, map(getObj, results.graph().nodes)))
        return({"nodes": nodes,
                "links": links})

    with driver.session() as session:
        resp = session.read_transaction(getParentFromGraph)
        return JsonResponse(resp)
    return JsonResponse({"1": "2"})


def addParents(request):
    inputNode = json.loads(request.body)['node']

    def getParentFromGraph(tx):
        results = tx.run(
            "match (c)<-[r]-(a)-[q]->(b) "
            " where b.languageCode = $languageCode"
            " and b.languageName = $languageName"
            " and b.latinTranscription = $latinTranscription"
            " return a,r,q",
            languageCode=inputNode['languageCode'], languageName=inputNode[
                'languageName'], latinTranscription=inputNode['latinTranscription']
        )

        graph = {}
        nodes = list(map(lambda e: e.id, results.graph().nodes))
        for node in nodes:
            graph[node] = []

        relationships = results.graph().relationships
        for relationship in relationships:
            nodesInRelationship = list(map(lambda e: e.id, relationship.nodes))
            graph[nodesInRelationship[0]].append(nodesInRelationship[1])

        def getObj(obj):
            o = {key: obj.get(key) for key in list(obj.keys())}
            if len(o) > 0:
                o.update({"id": obj.id})
                return o
            else:
                return None

        links = list(map(lambda e: {"source": e[0].id, "target": e[1].id, "type": e[2]}, map(
            lambda e: list(e.nodes) + [e.type], results.graph().relationships)))
        nodes = list(filter(lambda e: e, map(getObj, results.graph().nodes)))
        return({"nodes": nodes,
                "links": links})

    with driver.session() as session:
        resp = session.read_transaction(getParentFromGraph)
        return JsonResponse(resp)
    return JsonResponse({"1": "2"})


def expandNode(request):
    inputNode = json.loads(request.body)['node']
    returnCompounds = json.loads(request.body)['returnCompoundTerms']

    def getWordFromGraph(tx):
        results = tx.run(
            "match (a:word)-[r]->(b) "
            " where a.languageName=$languageName"
            " and a.languageCode = $languageCode"
            " and a.latinTranscription = $latinTranscription"
            f''' {' ' if returnCompounds else ' and NOT (b.latinTranscription contains " " or b.latinTranscription contains "-")' }'''
            " return b,r", languageCode=inputNode['languageCode'], languageName=inputNode[
                'languageName'], latinTranscription=inputNode['latinTranscription'])

        graph = {}
        nodes = list(map(lambda e: e.id, results.graph().nodes))
        for node in nodes:
            graph[node] = []

        relationships = results.graph().relationships
        for relationship in relationships:
            nodesInRelationship = list(map(lambda e: e.id, relationship.nodes))
            graph[nodesInRelationship[0]].append(nodesInRelationship[1])

        def getObj(obj):
            o = {key: obj.get(key) for key in list(obj.keys())}
            if len(o) > 0:
                o.update({"id": obj.id})
                return o
            else:
                return None

        links = list(map(lambda e: {"source": e[0].id, "target": e[1].id, "type": e[2]}, map(
            lambda e: list(e.nodes) + [e.type], results.graph().relationships)))
        nodes = list(filter(lambda e: e, map(getObj, results.graph().nodes)))
        return({"nodes": nodes,
                "links": links})

    with driver.session() as session:
        resp = session.read_transaction(getWordFromGraph)
        return JsonResponse(resp)
    return JsonResponse({"1": "2"})

def getLanguageCodeForLanguageName(languageName):
    match = linguisticsDB["languages"].find_one({"name": languageName})
    return match["code"]

def getAllLanguages(req):
    allLanguages = linguisticsDB["languages"].find()
    allLanguages = list(map(lambda e: [e["code"], e["name"]], allLanguages))
    return JsonResponse({"languages": allLanguages})
