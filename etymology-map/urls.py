from django.urls import path

from . import views

urlpatterns = [
    path('all-words', views.getAllWords, name='getAllWords'),
    path('root-word', views.getRootWord, name='getRootWord'),
    path('expand-node', views.expandNode, name='expandNode'),
    path('add-parents', views.addParents, name='addParents'),
    path('add-word', views.addWord, name='addWord'),
    path('add-etymology', views.getEtymology, name='getEtymology'),
    path('autocomplete', views.autocomplete, name='autocomplete'),
    path('doublets', views.getDoublets, name='getDoublets'),
    path('get-all-languages', views.getAllLanguages, name='getAllLanguages'),
    path('', views.index, name='index')
]
