from neo4j import GraphDatabase
from pymongo import MongoClient

uri = "bolt://localhost:7687"
driver = GraphDatabase.driver(uri, auth=("neo4j", "pass"))


MongoClient = MongoClient('localhost', 27017)
db = MongoClient.cache
collection = db["etymologyWords"]


def print_friends_of(tx, name):
    results = tx.run("MATCH (a:word)-[r:PARENT]->(:word)"
                     "RETURN a, r", name=name)
    for record in results:
        print(record["a"])
    print(results.data())
    print(results.values())
    print(list(map(lambda e: e.id, results.graph().nodes)))
    print(results.graph().relationships)
    print(list(map(lambda e: e.id, results.graph().relationships)))

    graph = {}
    nodes = list(map(lambda e: e.id, results.graph().nodes))
    for node in nodes:
        graph[node] = []

    relationships = results.graph().relationships
    for relationship in relationships:
        nodesInRelationship = list(map(lambda e: e.id, relationship.nodes))
        graph[nodesInRelationship[0]].append(nodesInRelationship[1])

        print(list(map(lambda e: e.id, relationship.nodes)))

    print(graph)


with driver.session() as session:
    session.read_transaction(print_friends_of, "elf")
