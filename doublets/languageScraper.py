import sys
import linecache
import requests
from bs4 import BeautifulSoup, Tag, NavigableString
import re
import json
import time
import datetime
import pymongo
import array
from pymongo import MongoClient
import random

# set mongo vars
client = MongoClient('localhost', 27017)
db = client.linguistics
collection = db["languages"]
outfile = open("output.txt", "a+", encoding="utf-8")
outfile.write("Test")
# set urls


def PrintException():
    exc_type, exc_obj, tb = sys.exc_info()
    f = tb.tb_frame
    lineno = tb.tb_lineno
    filename = f.f_code.co_filename
    linecache.checkcache(filename)
    line = linecache.getline(filename, lineno, f.f_globals)
    print('EXCEPTION IN ({}, LINE {} "{}"): {}'.format(
        filename, lineno, line.strip(), exc_obj))


languageListURL = "https://en.wiktionary.org/wiki/Wiktionary:List_of_languages"
language_request_object = requests.get(languageListURL)
language_response = BeautifulSoup(
    language_request_object.content, 'html.parser')

rows = language_response.find_all("tr")

for row in rows:
    languageCode = row.get("id")
    # print(row)
    cells = row.find_all("td")  # create list of cells
    if len(cells) > 0:
        # print(cells)
        languageName = cells[1].find("a").contents[0]
        languageCode = cells[0].find("code").getText()
        # print(languageName)
        # print(languageCode + " = " + languageName)
        collection.insert({"code": languageCode, "name": languageName})
