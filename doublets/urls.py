from django.urls import path

from . import views

urlpatterns = [
    path('get-all-languages', views.getAllLanguages, name='getAllLanguages'),
    path('', views.index, name='index'),
]
