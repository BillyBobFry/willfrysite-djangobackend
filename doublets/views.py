from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import json
import time
import datetime
import pymongo
import array
from pymongo import MongoClient
import xml.etree.ElementTree as ET
import requests
import re

client = MongoClient('localhost', 27017)
db = client.linguistics
collection = db["doublets"]

headers = {'content-type': 'text/xml'}

distinctLanguages = list(map(
    lambda e: {"code": e["code"], "name": e["name"]}, list(db["languages"].find())))
# for lang in distinctLanguages:
#     returnArray.append({"code": lang["code"], "name": lang["name"]})

doubletsForLanguages = list(
    collection.aggregate([{
        "$group": {
            "_id": {
                "rootID": "$rootID",
                "rootLatinTranscription": "$rootLatinTranscription",
                "lang": "$lang"
            },
            "count": {
                "$sum": 1
            }
        }
    }, {
        "$match": {
            "count": {
                "$gt": 1
            }
        }
    }, {
        "$group": {
            "_id": {
                "lang": "$_id.lang"
            },
            "count": {
                "$sum": 1
            }
        }
    }, {
        "$sort": {
            "count": -1
        }
    }]))

# join the two together
# print(doubletsForLanguages)
for language in distinctLanguages:
    matchingLanguage = list(filter(
        lambda x: (x["_id"]["lang"] == language["code"] or f'{x["_id"]["lang"]}-Latn' == language["code"]), doubletsForLanguages))
    language["count"] = matchingLanguage[0]["count"] if len(
        matchingLanguage) > 0 else 0

distinctLanguages = list(filter(lambda x: x["count"] > 0, distinctLanguages))
# print(doubletsForLanguages)


def getDoubletCounts():

    return doublets


def index(request):
    requestBody = json.loads(request.body)
    requestType = requestBody['requestType']
    # print(requestType)
    if requestType == "getDoublets":
        returnVar = []
        requestRoot = requestBody['rootWord']
        requestLang = requestBody['lang']

        # filter function to return doublets
        def filterDoublets(cognate):
            if (cognate["lang"] == requestLang):
                return True
            else:
                return False

        print(requestLang)
        print(requestRoot)
        endWord = list(
            collection.find({
                "lang": requestLang,
                "rootLatinTranscription": requestRoot
            }))[0]
        print(endWord)

        rootID = endWord["rootID"]

        # get all words that share the same root
        cognates = list(collection.find({"rootID": rootID}))

        # get all doublets (same language cognates)
        doublets = filter(filterDoublets, cognates)
        for doublet in doublets:
            thisWord = doublet
            returnVar.append(doublet)
            while thisWord["parentID"]:
                for word in cognates:
                    if word["_id"] == thisWord["parentID"]:
                        thisWord = word
                        try:
                            returnVar.index(word)
                        except:  # only add word if it doesn't exist. sorry.
                            returnVar.append(word)
                            pass

        print(returnVar)
        for word in returnVar:
            if word["lang"] == "ine-pro":
                print(word["latinTranscription"])
            word["_id"] = str(word["_id"])
            word["parentID"] = str(word["parentID"])
    # print(list(doublets))

        return JsonResponse({"results": returnVar})

    elif requestType == "getDoubletCounts":
        lang = requestBody["lang"]
        print(requestBody)
        returnRoots = requestBody["returnRoots"]

        doublets = list(
            collection.aggregate([{
                "$match": {
                    "lang": lang
                }
            }, {
                "$group": {
                    "_id": {
                        "rootID": "$rootID",
                        "rootLatinTranscription": "$rootLatinTranscription",
                        "lang": "$lang"
                    },
                    "count": {
                        "$sum": 1
                    },
                    "doublets": {
                        "$push": {
                            "latinTranscription": "$latinTranscription"
                        }
                    }
                }
            }, {
                "$match": {
                    "count": {
                        "$gt": 1
                    }
                }
            }, {
                "$sort": {
                    "count": -1
                }
            }]))

        def filterRoots(doublet):
            print(doublet["_id"]["rootLatinTranscription"])
            if doublet["_id"]["rootLatinTranscription"][-1] == "-":
                return False
            else:
                return True

        print(returnRoots)
        if returnRoots == False:
            doublets = list(filter(filterRoots, doublets))

        for doublet in doublets:
            try:
                doublet["_id"]["rootID"] = str(doublet["_id"]["rootID"])
            except:
                pass
        return JsonResponse({"results": doublets})

    elif requestType == "getDoubletCountsForLanguages":
        # doubletCounts = getDoubletCounts()
        returnVar = []
        returnVar = list(
            collection.aggregate([{
                "$group": {
                    "_id": {
                        "rootID": "$rootID",
                        "rootLatinTranscription": "$rootLatinTranscription",
                        "lang": "$lang"
                    },
                    "count": {
                        "$sum": 1
                    }
                }
            }, {
                "$match": {
                    "count": {
                        "$gt": 1
                    }
                }
            }, {
                "$group": {
                    "_id": {
                        "lang": "$_id.lang"
                    },
                    "count": {
                        "$sum": 1
                    }
                }
            }, {
                "$sort": {
                    "count": -1
                }
            }]))

        # print(testvar)
        return JsonResponse({"results": returnVar})

        # for el in doubletCounts:
        #     i = returnVar.index()

    else:
        words = collection.find()
        returnVar = []
        for word in words:
            returnWord = {}
            returnWord["id"] = str(word["_id"])
            returnWord["parentID"] = str(word["parentID"])
            returnWord["lang"] = word["lang"]
            returnWord["latinTranscription"] = word["latinTranscription"]
            returnWord["ipaTranscription"] = word["ipaTranscription"]
            returnWord["rootLatinTranscription"] = word[
                "rootLatinTranscription"]
            returnVar.append(returnWord)

        return JsonResponse({"results": returnVar})


def getAllLanguages(request):
    return JsonResponse({"results": distinctLanguages})
