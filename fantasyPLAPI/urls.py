from django.urls import path, re_path

from . import views

urlpatterns = [
    path('get-static-data',
         views.getStaticData, name='getStaticData'),
    path('get-player-data/<int:playerID>',
         views.getPlayerData, name='getPlayerData'),
    path('', views.index, name='index'),
]
