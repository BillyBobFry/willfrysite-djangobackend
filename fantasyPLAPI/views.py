from django.shortcuts import render
from django.http import JsonResponse
import json
import django.middleware.csrf
import requests
import datetime
from pymongo import MongoClient

client = MongoClient('localhost', 27017)
db = client.fantasyPL
fplYear = 2020


# Create your views here.
def index(request):

    if request.method == "GET":
        csrfToken = django.middleware.csrf.get_token(request)
        response = JsonResponse({})
        response["Access-Control-Allow-Headers"] = "X-CSRFToken, Origin"
        response["Access-Control-Allow-Methods"] = "POST, GET, OPTIONS"
        response["Access-Control-Allow-Origin"] = request.META.get(
            "HTTP_ORIGIN")
        response["Access-Control-Allow-Credentials"] = "true"
        return response

    if request.method == "POST":
        URL = json.loads(request.body)['url']

        r = requests.get(url=URL)

        print(r)
        data = r.json()
        response = JsonResponse(data)
        response["Access-Control-Allow-Origin"] = request.META.get(
            "HTTP_ORIGIN")
        response["Access-Control-Allow-Credentials"] = "true"
        return response


def getStaticDataFromFPL():
    staticDataURL = "https://fantasy.premierleague.com/api/bootstrap-static/"
    r = requests.get(url=staticDataURL)
    r = r.json()
    r["players"] = r["elements"]
    r["positions"] = r["element_types"]
    return r


def getPlayerDataFromFPL(playerID):
    playerDataURL = f"https://fantasy.premierleague.com/api/element-summary/{playerID}/"
    r = requests.get(url=playerDataURL)
    r = r.json()
    return r


def getPlayerData(req, playerID):
    minFreshness = datetime.timedelta(hours=1)
    collection = db[f"playerData{fplYear}"]
    now = datetime.datetime.now()

    playerData = list(collection.find({"id": playerID}))
    if len(playerData) > 0 and now - playerData[0]["time"] > minFreshness:
        playerData = playerData[0]["data"]
    else:
        playerDataKeys = ["history"]
        playerDataHistoryKeys = ["total_points", "element", "round"]
        playerData = getPlayerDataFromFPL(playerID)
        playerDataJSON = {"time": datetime.datetime.now(),
                          "data": playerData}
        playerData["history"] = list(
            map(lambda e: {key: e[key] for key in playerDataHistoryKeys}, playerData["history"]))
        collection.insert(playerDataJSON)

    playerData = {"history": playerData["history"]}

    return JsonResponse(playerData)


def getStaticData(req):
    minFreshness = datetime.timedelta(hours=1)
    collection = db[f"staticData{fplYear}"]
    latestData = list(collection.find())
    if len(latestData) > 0:
        latestData = latestData[0]
    else:
        latestData = {"time": datetime.datetime.now(),
                      "data": getStaticDataFromFPL()}

    dataTime = latestData["time"]
    now = datetime.datetime.now()

    staticData = {"teams": [], "players": [], "data": []}
    staticData["data"] = latestData["data"] if now - \
        dataTime > minFreshness else getStaticDataFromFPL()

    teamKeys = ["code", "id", "name", "short_name"]
    staticData["teams"] = list(map(
        lambda e: {key: e[key] for key in teamKeys}, staticData["data"]["teams"]))

    playerKeys = ["id", "web_name", "total_points", "team",
                  "team_code", "now_cost", "element_type"]
    staticData["players"] = list(map(
        lambda e: {key: e[key] for key in playerKeys}, staticData["data"]["players"]))

    positionKeys = ["id", "plural_name", "singular_name_short"]
    staticData["positions"] = list(map(
        lambda e: {key: e[key] for key in positionKeys}, staticData["data"]["positions"]))
    collection.update({}, {"$set": {"data": staticData,
                                    "time": datetime.datetime.now()}}, upsert=True)

    return JsonResponse({"teams": staticData["teams"], "players": staticData["players"], "positions": staticData["positions"]})
