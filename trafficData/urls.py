from django.urls import path, re_path

from . import views

urlpatterns = [
	path('', views.index, name='index'),
	re_path('.*\.png', views.index, name='simpsons'),
]