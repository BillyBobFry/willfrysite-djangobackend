from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import json
import time
import datetime
import array
import requests
import csv
import re

headers = {'content-type': 'text/xml'}

# results = {"min": 0, "max": 999, dataa: []}


class results:
    def __init__(self):
        self.min = 0
        self.max = 400
        self.data = []


dailyResults = results()
dailyResults.min = 0
dailyResults.max = 999
dailyResults.data = {}

monthData = []
for i in range(13):
    monthData.append(results())
    monthData[i].min = 0
    monthData[i].max = 30

yearData = results()
yearData.min = 0
yearData.max = 30


def getFreshMonthData():
    if len(monthData[0].data) == 0:
        r = requests.get(
            url="https://www.metoffice.gov.uk/hadobs/hadcet/cetml1659on.dat")
        cr = r.content.decode('utf-8').splitlines()
        for row in cr:
            matches = re.findall(r"\b\d+\.?\d+\b", row)
            if len(matches) > 12:
                yearColumn = matches[0]
                for monthColumn in range(12):
                    month = "0" + str(int(monthColumn) + 1) if int(
                        monthColumn) < 9 else int(monthColumn) + 1

                    tempValue = float(matches[monthColumn + 1])
                    if tempValue < 99:
                        monthData[0].data.append(
                            [int(str(yearColumn) + str(month)), tempValue])
                        monthData[monthColumn + 1].data.append(
                            [int(yearColumn), tempValue])
                yearTemp = float(matches[13])
                if yearTemp < 99:
                    yearData.data.append([int(yearColumn), yearTemp])

        def sortByDate(val):
            return val[0]

        def returnSecond(ar):
            return ar[1]

        monthData[0].data.sort(key=sortByDate)
        monthData[0].max = max(map(returnSecond, monthData[0].data))
        monthData[0].min = min(map(returnSecond, monthData[0].data))
        yearData.max = max(map(returnSecond, yearData.data))
        yearData.min = min(map(returnSecond, yearData.data))


getFreshMonthData()


def getDailyResults(request):
    requestBody = json.loads(request.body)
    requestedDate = requestBody['date']
    requestedMonth = int(requestedDate[:2])
    requestedDay = int(requestedDate[2:])
    if dailyResults.data == {}:
        r = requests.get(
            url="https://www.metoffice.gov.uk/hadobs/hadcet/cetmaxdly1878on_urbadj4.dat"
        )
        cr = r.content.decode('utf-8').splitlines()
        for row in cr:
            matches = re.findall(r"\b\d+\b", row)
            yearColumn = matches[0]
            dayColumn = matches[1]
            if yearColumn not in dailyResults.data.keys():
                dailyResults.data[yearColumn] = []
                for i in range(12):
                    dailyResults.data[yearColumn].append([])

            for monthColumn in range(12):
                day = "0" + str(dayColumn) if int(
                    dayColumn) < 10 else dayColumn
                month = "0" + str(int(monthColumn) + 1) if int(
                    monthColumn) < 9 else int(monthColumn) + 1

                tempValue = int(matches[int(month) + 1])
                if tempValue < 999:
                    dailyResults.data[yearColumn][monthColumn].append(
                        tempValue)

    returnVar = []
    for i in dailyResults.data:
        if len(dailyResults.data[i][requestedMonth]) > 0:
            returnVar.append([
                int(i),
                dailyResults.data[i][requestedMonth][requestedDay - 1] / 10
            ])
    return JsonResponse({"results": {"max": 30, "min": 0, "data": returnVar}})


def getMonthData(request):
    requestBody = json.loads(request.body)
    requestedMonth = requestBody['requestedMonth']

    if requestedMonth == 13:
        return getYearData(request)
    else:
        return JsonResponse({
            "results": {
                "max": monthData[requestedMonth].max,
                "min": monthData[requestedMonth].min,
                "data": monthData[requestedMonth].data
            }
        })


def getYearData(request):

    return JsonResponse({
        "results": {
            "max": yearData.max,
            "min": yearData.min,
            "data": yearData.data
        }
    })


def index(request):
    requestBody = json.loads(request.body)
    requestType = requestBody['requestType']

    return JsonResponse({
        "results": {
            "max": dailyResults.max,
            "min": dailyResults.min,
            "data": dailyResults.data
        }
    })
