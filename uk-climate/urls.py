from django.urls import path

from . import views

urlpatterns = [
    path('getYearData', views.getYearData, name='getYearData'),
    path('getMonthData', views.getMonthData, name='getMonthData'),
    path('getDailyResults', views.getDailyResults, name='getDailyResults'),
    path('', views.index, name='index'),
]