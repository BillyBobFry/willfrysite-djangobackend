from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import json
import time
import datetime
import array
import requests
import re
from statistics import mode
from pymongo import MongoClient

client = MongoClient('localhost', 27017)
db = client.scriptCache

headers = {'content-type': 'text/xml'}
elasticURL = "http://localhost:9200"
groupCountByLine = False


def performFreetextSearch(show, term):
    queryURL = f'{elasticURL}/{show}/scripts/_search?size=10000'
    queryJSON = {
        "query": {
            "match_phrase": {
                "line": {
                    "query": term
                }
            }
        }
    }
    r = requests.post(url=queryURL, json=queryJSON)
    hits = json.loads(r.text)["hits"]["hits"]
    print(hits)
    return hits


def getTermByCharacter(request):
    requestBody = json.loads(request.body)
    freetextTerm = requestBody["freetextTerm"].lower().strip()
    show = requestBody["show"]

    now = datetime.datetime.now()

    collection = db[show]
    returnObj = []
    groupSwitch = {"trek": "show"}

    # see if we can find a cached result for this
    cachedTerm = collection.find({"term": freetextTerm})
    if len(list(cachedTerm)) == 0:  # if no cached result, query elastic directly
        hits = performFreetextSearch(show, freetextTerm)
        results = list(map(lambda e: e["_source"], hits))
        uniqueCharacters = list(
            set(map(lambda e: e["_source"]["character"], hits)))

        for character in uniqueCharacters:
            characterObj = {"character": character}
            try:
                group = groupSwitch[show]
                try:
                    characterObj["group"] = mode(map(lambda e: e["_source"][group], filter(
                        lambda e: e["_source"]["character"] == character, hits)))
                except:  # >1 mode
                    characterObj["group"] = list(filter(
                        lambda e: e["_source"]["character"] == character, hits))[0]["_source"][show]
                    pass
            except:  # no grouping
                pass

            if groupCountByLine:
                characterObj["count"] = len(
                    list(filter(lambda e: e["_source"]["character"] == character, hits)))
            else:
                total = 0
                characterLines = list(filter(
                    lambda e: e["_source"]["character"] == character, hits))
                pattern = r"" + freetextTerm + ""
                for line in characterLines:
                    totalInLine = len(re.findall(
                        pattern, line["_source"]["line"].lower()))
                    total += totalInLine

                characterObj["count"] = total

            returnObj.append(characterObj)

        # add result to the cache for next time
        collection.insert(
            {"term": freetextTerm, "lastSearched": now, "count": 1, "returnObj": returnObj})
    else:  # use the cached term
        collection.update({"term": freetextTerm}, {
                          "$inc": {"count": 1}, "$set": {"lastSearched": now}})
        cachedTerm = collection.find({"term": freetextTerm})
        returnObj = list(cachedTerm)[0]["returnObj"]

    return JsonResponse({
        "phrase": freetextTerm,
        "results": returnObj
    })


def getSignificantTermsByCharacter(request):
    returnObj = []
    queryURL = f'{elasticURL}?size=1000'
    queryJSON = {
        "query": {
            "match": {"character": "NOG"}
        },
        "aggregations": {
            "my_sample": {
                "sampler": {
                    "shard_size": 100
                },
                "aggregations": {
                    "keywords": {
                        "significant_text": {"field": "line"}
                    }
                }
            }
        }
    }
    queryJSON = {
        "query": {
            "match_all": {}
        },
        "aggs": {
            "line_agg": {
                "terms": {"field": "line"}
            }
        }
    }
    r = requests.post(url=queryURL, json=queryJSON)
    print(json.loads(r.text))
    hits = json.loads(r.text)

    return JsonResponse({
        "results": hits
    })


def index(request):
    # requestBody = json.loads(request.body)
    # print("HERE")

    # queryURL = f'{elasticURL}?size=5'
    # queryJSON = {
    #     "query": {
    #         "match": {
    #             "line": {
    #                 "query": "deflectors"
    #             }
    #         }
    #     }
    # }
    # r = requests.post(url=queryURL, json=queryJSON)
    # print(r.text)
    # hits = json.loads(r.text)["hits"]["hits"]
    # print("-----------------")
    # print(hits)
    # for hit in hits:
    #     print(hit["_source"])
    #     # print(hit["line"])

    return JsonResponse({
        "results": "success"
    })
